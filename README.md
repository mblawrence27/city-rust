# CitySynth

A polyphonic software synthesiser thats wild but warm offering unrivaled discrete harmonic oscillator control.

CitySynth offers both modulating algorithmic reverb and delay, buttery filters and oscillator level analogue-style end stage.

Try out the (very) work in progress live [demo](https://michaelblawrence.github.io/rustwasmtester/react/). Try pressing some keyboard keys with your 🔊 turned up!