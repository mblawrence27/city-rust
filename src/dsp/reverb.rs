use crate::dsp::delay::Delay;
use crate::utils::WrappingIndex;
use crate::rand_core::{RngCore, SeedableRng};

use std::iter;

const MAX_PRE_DELAY_SECONDS: f64 = 0.2;
const REVERB_CONSTANT_G: f64 = 0.708;
const REVERB_CONSTANT_G2: f64 = 1.0 - REVERB_CONSTANT_G * REVERB_CONSTANT_G;

pub struct Reverb {
    sr: i32,
    enable_bypass: bool,
    delays: Vec<Delay>,
    delays_count: usize,
    pre_delay_buffer: Vec<f64>,
    pre_delay_indexer: WrappingIndex,
    pre_delay_len: f64,
}

impl Reverb {
    pub fn new(sr: i32, buffer_lengths: Vec<usize>) -> Reverb {
        let delays: Vec<Delay> = buffer_lengths
            .iter()
            .map(|len| Delay::new(sr, *len, 1.0))
            .collect();
        let delays_count = delays.len();

        let pre_buffer_len = (f64::from(sr) * MAX_PRE_DELAY_SECONDS) as usize;
        let pre_delay_buffer = iter::repeat(0.0).take(pre_buffer_len).collect();
        let pre_delay_indexer = WrappingIndex::new(0.0);

        Reverb {
            sr,
            enable_bypass: false,
            delays,
            delays_count,
            pre_delay_buffer,
            pre_delay_indexer,
            pre_delay_len: 0.0,
        }
    }

    pub fn get_sample(&mut self, input_sample: f64, wet_mix: f64) -> f64 {
        if self.enable_bypass {
            return input_sample;
        }

        let mut output_sample = if self.pre_delay_len.is_normal() {
            self.pre_delay_buffer[self.pre_delay_indexer.idx()] = input_sample;
            self.pre_delay_indexer
                .wrap_increment(1.0, self.pre_delay_len);

            self.pre_delay_buffer[self.pre_delay_indexer.idx()]
        } else {
            input_sample
        };

        for i in 0..self.delays_count {
            let delayed_sample = self.get_delay_sample(i);
            self.send_delay_sample(i, output_sample + delayed_sample * REVERB_CONSTANT_G);
            output_sample =
                -output_sample * REVERB_CONSTANT_G + REVERB_CONSTANT_G2 * delayed_sample;
        }

        -wet_mix * output_sample + (1.0 - wet_mix) * input_sample
    }

    fn get_delay_sample(&self, delay_idx: usize) -> f64 {
        self.delays[delay_idx].return_sample()
    }

    fn send_delay_sample(&mut self, delay_idx: usize, sample: f64) {
        self.delays[delay_idx].send_sample(sample);
    }

    pub fn set_pre_delay_seconds(&mut self, seconds: f64) {
        assert!(
            seconds <= MAX_PRE_DELAY_SECONDS,
            "the length of the pre delay must be less than {} seconds",
            MAX_PRE_DELAY_SECONDS
        );

        self.pre_delay_len = seconds * f64::from(self.sr);
        self.pre_delay_indexer.reset_wrap_len(self.pre_delay_len)
    }

    pub fn set_nested_delays_flutter(&mut self, flutter: f64) {
        for delay in self.delays.iter_mut() {
            delay.flutter = flutter;
        }
    }

    pub fn get_nested_delays_flutter(&mut self) -> f64 {
        self.delays
            .iter()
            .map(|delay: &Delay| delay.flutter)
            .sum::<f64>()
            / self.delays.len() as f64
    }

    pub fn set_nested_delays_flutter_speed(&mut self, flutter_speed: f64) {
        for delay in self.delays.iter_mut() {
            delay.flutter_speed = flutter_speed;
        }
    }

    pub fn get_nested_delays_flutter_speed(&mut self) -> f64 {
        self.delays
            .iter()
            .map(|delay: &Delay| delay.flutter_speed)
            .sum::<f64>()
            / self.delays.len() as f64
    }

    pub fn generate_delay_lens(nested_size: usize, sr: f64, tau_seed: f64, dran_offset: f64) -> Vec<usize> {
        let nested_size = nested_size as i32;
        // TODO: use thread prng where available (native), fall back to xor based otherwise (web)
        let mut rng = rand_xorshift::XorShiftRng::from_seed([3, 1, 2, 4, 2, 3, 4, 2, 3, 4, 5, 2, 3, 4, 5, 6]);
        
        let mut next_f64 = || rng.next_u64() as f64 / std::u64::MAX as f64 - 0.5;

        let attempt_limit = 200;
        let ran = 0.023;
        let dran = dran_offset - ran;

        let mut nested_lens = vec![];
        let mut idx: i32 = 0;
        loop {
            let tau = ((tau_seed).powf(nested_size.into()) - 1.0) / 1000.0;

            let mut attempts = 0;
            let mut exit = false;
            let mut t1 = sr * tau;
            
            t1 += 0.01 * t1 * (2.0 * (0.5 * next_f64() - 1.0));

            let t1_i32 = t1 as u32;
            nested_lens.push(t1_i32);
            
            while idx < nested_size && !exit {
                loop {
                    let ranfact = dran + ran * next_f64();
                    t1 /= ranfact;
                    let len = t1 as u32;
                    if len == 0 {
                        exit = true;
                        break;
                    }
                    if t1_i32 % len == 0 {
                        continue;
                    }
                    nested_lens.push(len);
                    attempts += 1;
                    if attempts >= attempt_limit {
                        exit = true;
                        idx = -1;
                        nested_lens.clear();
                        break;
                    }
                    break;
                }
                idx += 1;
            }
            if idx >= nested_size {
                break;
            }
        }
        nested_lens.iter().map(|len| *len as usize).collect()
    }

    pub fn compute_delay_seconds(
        reverb: &mut Reverb,
        min_seconds_of_silence: f64,
        slience_db_threshold: f64,
    ) -> ReverbSimulationResults {
        assert!(
            min_seconds_of_silence >= 0.0,
            "the provided number seconds of silence must be positive"
        );
        assert!(slience_db_threshold < 0.0,
            "the provided decibel level below for silence/signal determination must below unit level (0 dB)");
        let _ = reverb.get_sample(1.0, 1.0);

        let rel_threshold = (10.0_f64).powf(slience_db_threshold / 10.0);

        let mut max_sample_seen = 0.0;
        let mut zero_samples = 0;
        let mut first_non_zero_index: i32 = -1;
        let mut idx = 0;

        loop {
            let sample = reverb.get_sample(0.0, 1.0);
            if sample > max_sample_seen * rel_threshold {
                zero_samples = 0;
                if sample > max_sample_seen {
                    max_sample_seen = sample;
                }
                if first_non_zero_index < 0 {
                    first_non_zero_index = idx;
                }
            } else {
                if first_non_zero_index >= 0 {
                    zero_samples += 1;
                }
                if f64::from(zero_samples) >= min_seconds_of_silence * f64::from(reverb.sr) {
                    let non_zeros = f64::from(idx) - f64::from(zero_samples);
                    let run_time = non_zeros - f64::from(first_non_zero_index);
                    let seconds_of_reverb = run_time / f64::from(reverb.sr);
                    let time_of_last_signal = non_zeros / f64::from(reverb.sr);
                    println!("we found {0} contiguous zero samples starting at index #{1} (runtime: {2} samples = {3:.5} seconds)",
                            zero_samples,
                            non_zeros,
                            run_time,
                            seconds_of_reverb);
                    return ReverbSimulationResults {
                        seconds_of_reverb,
                        time_of_last_signal,
                    };
                }
            }
            idx += 1;
        }
    }
}

impl Default for Reverb {
    fn default() -> Reverb {
        Reverb::new(44100, vec![3501, 130, 43, 14, 4])
    }
}

pub struct ReverbSimulationResults {
    pub seconds_of_reverb: f64,
    pub time_of_last_signal: f64,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reverb_can_prolong_input_pulse() {
        let sr = 44100;
        let min_seconds_of_silence = 1.0;
        let slience_db_threshold = -30.0;

        let mut reverb = Reverb::new(sr, vec![3501, 130, 43, 14, 4]);
        let results = Reverb::compute_delay_seconds(
            &mut reverb,
            min_seconds_of_silence,
            slience_db_threshold,
        );

        let expected_seconds_of_reverb: f64 = 1.5;
        assert_eq!(
            (expected_seconds_of_reverb * 10.0).trunc() as u32 * 100,
            (results.seconds_of_reverb * 10.0).trunc() as u32 * 100
        );
        assert_eq!(
            (results.seconds_of_reverb * 10.0).trunc() as u32 * 100,
            (expected_seconds_of_reverb * 1000.0) as u32
        );
    }

    #[test]
    fn reverb_can_pre_delay_initial_signal() {
        let sr = 44100;
        let min_seconds_of_silence = 1.0;
        let slience_db_threshold = -30.0;

        let mut reverb = Reverb::new(sr, vec![3501, 130, 43, 14, 4]);
        let pre_delay_seconds = 0.2;
        reverb.set_pre_delay_seconds(pre_delay_seconds);
        let results = Reverb::compute_delay_seconds(
            &mut reverb,
            min_seconds_of_silence,
            slience_db_threshold,
        );

        let expected_seconds_of_reverb = 1.5;
        let expected_seconds = expected_seconds_of_reverb + pre_delay_seconds;
        assert_eq!(
            (expected_seconds_of_reverb * 10.0).trunc() as u32 * 100,
            (results.seconds_of_reverb * 10.0).trunc() as u32 * 100
        );
        assert_eq!(
            (results.time_of_last_signal * 10.0).trunc() as u32 * 100,
            (expected_seconds * 1000.0) as u32
        );
    }
}