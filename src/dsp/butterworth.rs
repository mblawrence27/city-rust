use crate::math::polynomials::{PolynomialMultiplier, PolynomialResult};
use crate::num::complex::Complex64;
use crate::num::One;
use std::f64::consts::PI;

const MAX_POLES: usize = 4;
const FILTER_FEEDBACK: f64 = 0.0;
const FILTER_DRIVE: f64 = 1.0;

#[derive(Debug)]
pub struct ButterworthFilter {
    sr: f64,
    fc: Option<f64>,
    poles: usize,
    past_x: [f64; MAX_POLES],
    past_y: [f64; MAX_POLES],
    iir_coefs_a: [f64; MAX_POLES + 1],
    iir_coefs_b: [f64; MAX_POLES + 1],
}

impl ButterworthFilter {
    pub fn new(poles: usize, sr: f64) -> ButterworthFilter {
        let (past_x, past_y) = (
            ButterworthFilter::const_zeros(),
            ButterworthFilter::const_zeros(),
        );
        let (iir_coefs_a, iir_coefs_b) = (
            ButterworthFilter::const_zeros_c(),
            ButterworthFilter::const_zeros_c(),
        );
        ButterworthFilter {
            sr,
            fc: None,
            poles,
            past_x,
            past_y,
            iir_coefs_a,
            iir_coefs_b,
        }
    }

    pub fn from(
        poles: usize,
        sr: f64,
        iir_coefs_a: [f64; MAX_POLES + 1],
        iir_coefs_b: [f64; MAX_POLES + 1],
    ) -> ButterworthFilter {
        let (past_x, past_y) = (
            ButterworthFilter::const_zeros(),
            ButterworthFilter::const_zeros(),
        );
        ButterworthFilter {
            sr,
            fc: Some(0.0),
            poles,
            past_x,
            past_y,
            iir_coefs_a,
            iir_coefs_b,
        }
    }

    pub fn coefs(&self) -> ([f64; MAX_POLES + 1], [f64; MAX_POLES + 1]) {
        (self.iir_coefs_a, self.iir_coefs_b)
    }

    pub fn process(&mut self, x: f64) -> f64 {
        let x = x + self.past_y[self.poles - 1] * FILTER_FEEDBACK;
        let mut y = x * self.iir_coefs_b[0];
        for i in 1..=self.poles {
            y += self.past_x[self.poles - i] * self.iir_coefs_b[i];
            y -= self.past_y[self.poles - i] * self.iir_coefs_a[i];
        }
        for i in 1..self.poles {
            self.past_x[i - 1] = self.past_x[i];
            self.past_y[i - 1] = self.past_y[i];
        }
        if y.is_nan() || y.is_infinite() {
            y = 0.0;
        }
        self.past_x[self.poles - 1] = x;
        self.past_y[self.poles - 1] = y;

        y *= FILTER_DRIVE;
        y = if y > 1.0 {
            1.0
        } else if y < -1.0 {
            -1.0
        } else {
            y
        };
        y *= PI / 2.0;
        y.sin() / FILTER_DRIVE
    }

    pub fn set_cutoff(&mut self, fc: f64) {
        if self.fc.is_none() || (self.fc.unwrap() - fc).abs() > std::f64::EPSILON {
            self.fc = Some(fc);
            let len = self.poles + 1;
            let (iir_coefs_a, iir_coefs_b) =
                ButterworthFilter::generate_iir_cooefs(self.poles as i32, fc, self.sr);
            self.iir_coefs_b[..len].clone_from_slice(&iir_coefs_b[..len]);
            self.iir_coefs_a[..len].clone_from_slice(&iir_coefs_a[..len]);
        }
    }

    pub fn generate_iir_cooefs(poles: i32, fc: f64, sr: f64) -> (Vec<f64>, Vec<f64>) {
        let omegac = ButterworthFilter::normalise_freq(fc, sr);

        let (iir_coefs_a, h0z_denom) = ButterworthFilter::calculate_hz_denom(poles, omegac);

        let h0z = (-omegac).powi(poles) / h0z_denom;

        let iir_coefs_b = ButterworthFilter::calculate_hz_numer(poles, h0z);
        (iir_coefs_a, iir_coefs_b)
    }

    fn calculate_hz_numer(poles: i32, h0z: f64) -> Vec<f64> {
        let hz_numer_factor = PolynomialResult::from(&vec![Complex64::one(), Complex64::one()]);
        let mut hz_numer_polyn =
            PolynomialMultiplier::from(vec![Complex64::from(h0z), Complex64::from(h0z)]);
        hz_numer_polyn.set_start_index(-poles);

        for _ in 1..poles {
            hz_numer_polyn *= &hz_numer_factor;
        }

        hz_numer_polyn.result().re()
    }

    fn calculate_hz_denom(poles: i32, omegac: f64) -> (Vec<f64>, f64) {
        let mut h0z_denom = 1.0;
        let mut hz_denom_poyln = PolynomialMultiplier::new(None, Some(-poles));

        let n_f64 = f64::from(poles);
        for k in 0..poles / 2 {
            let k_f64 = f64::from(k);
            let pk_even =
                Complex64::from_polar(&omegac, &((2.0 * k_f64 + n_f64 + 1.0) * PI / (2.0 * n_f64)));
            let pk_odd =
                Complex64::from_polar(&omegac, &(((1.5 * n_f64 - 1.0 * k_f64) - 0.5) * PI / n_f64));

            hz_denom_poyln *= &PolynomialResult::from(&vec![
                Complex64::one(),
                -(Complex64::one() + pk_even) / (Complex64::one() - pk_even),
            ]);
            hz_denom_poyln *= &PolynomialResult::from(&vec![
                Complex64::one(),
                -(Complex64::one() + pk_odd) / (Complex64::one() - pk_odd),
            ]);
            h0z_denom *= (1.0 - pk_even.re) * (1.0 - pk_even.re) - (pk_even.im * pk_odd.im);
        }
        if poles % 2 == 1 {
            let pk_last = Complex64::from_polar(
                &omegac,
                &((2.0 * (n_f64 - 1.0) + n_f64 + 1.0) * PI / (2.0 * n_f64)),
            );
            hz_denom_poyln *= &PolynomialResult::from(&vec![
                Complex64::one(),
                -(Complex64::one() + pk_last) / (Complex64::one() - pk_last),
            ]);
            h0z_denom *= 1.0 - pk_last.re;
        }

        (hz_denom_poyln.result().re(), h0z_denom)
    }

    pub fn const_zeros() -> [f64; MAX_POLES] {
        [0.0, 0.0, 0.0, 0.0]
    }

    pub fn const_zeros_c() -> [f64; MAX_POLES + 1] {
        [0.0, 0.0, 0.0, 0.0, 0.0]
    }

    fn normalise_freq(hz: f64, sr: f64) -> f64 {
        (PI * hz / sr).tan()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::helpers::compare_f64;

    const SAMPLE_RATE: f64 = 96000.0;

    #[test]
    fn butterworth_filter_can_create() {
        let filter = ButterworthFilter::new(4, SAMPLE_RATE);

        assert_eq!(filter.coefs().0.len(), 5);
        assert_eq!(filter.coefs().1.len(), 5);
    }

    #[test]
    fn butterworth_filter_can_process_input() {
        let iir_a = [
            1.0,
            -3.924748636922391,
            5.7770643720276649,
            -3.7798213918010455,
            0.9275063192535371,
        ];
        let iir_b = [
            4.1409860310320426E-08,
            1.6563944124128171E-07,
            2.4845916186192253E-07,
            1.6563944124128171E-07,
            4.1409860310320426E-08,
        ];
        let mut filter = ButterworthFilter::from(4, SAMPLE_RATE, iir_a, iir_b);

        let s = filter.process(1.0);

        assert!(compare_f64(s, 6.6E-08));
    }

    #[test]
    fn butterworth_filter_can_calc_coefs() {
        let fc = 1500.0;
        let iir_a = [
            1.0,
            -3.7435067617389679,
            5.2629037982333227,
            -3.2929432846648403,
            0.77362821946596361,
        ];
        let iir_b = [
            5.1232059674179028E-06,
            2.0492823869671611E-05,
            3.0739235804507415E-05,
            2.0492823869671611E-05,
            5.1232059674179028E-06,
        ];

        let mut filter = ButterworthFilter::new(4, SAMPLE_RATE);
        filter.set_cutoff(fc);

        let coefs = (
            filter
                .coefs()
                .0
                .iter()
                .zip(&iir_a)
                .all(|(c, i)| compare_f64(*c, *i)),
            filter
                .coefs()
                .1
                .iter()
                .zip(&iir_b)
                .all(|(c, i)| compare_f64(*c, *i)),
        );
        assert!(coefs.0);
        assert!(coefs.1);
    }

    #[test]
    fn butterworth_filter_can_normalise() {
        let fc = 1500.0;
        let sr = 44100.0;

        let tanned = ButterworthFilter::normalise_freq(fc, sr);

        assert!(compare_f64(tanned, 0.107265471));
    }
}