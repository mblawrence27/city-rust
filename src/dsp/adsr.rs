use crate::dsp::adsr::helpers::*;

const DEFAULT_SAMPLE_RATE: f64 = 44100.0;
type CFloat = f64;

#[derive(Debug)]
pub struct ADSR {
    state: EnvelopeState,
    position: usize,
    ampl: f64,
    prev: f64,
    output: f64,
    hold_level: f64,
    sample_rate: CFloat,
    attack_samples: usize,
    decay_samples: usize,
    sustain_level: CFloat,
    release_samples: usize,
}

#[derive(Debug, PartialEq)]
enum EnvelopeState {
    AwaitingKeyPress,
    AttackRamping,
    DecayRamping,
    Sustaining,
    ReleaseRamping,
}

impl Default for ADSR {
    fn default() -> ADSR {
        ADSR {
            state: EnvelopeState::AttackRamping,
            position: 0,
            ampl: 0.0,
            prev: 1.0,
            output: 0.0,
            hold_level: 0.0,
            sample_rate: DEFAULT_SAMPLE_RATE,
            attack_samples: 1,
            decay_samples: 1,
            sustain_level: 1.0,
            release_samples: 1,
        }
    }
}

impl ADSR {
    pub fn new(
        sample_rate: CFloat,
        attack_time: CFloat,
        decay_time: CFloat,
        sustain_level: CFloat,
        release_time: CFloat,
    ) -> ADSR {
        let attack_samples = to_samples(attack_time, sample_rate);
        let decay_samples = to_samples(decay_time, sample_rate);
        let release_samples = to_samples(release_time, sample_rate);
        ADSR {
            sample_rate,
            attack_samples,
            decay_samples,
            sustain_level,
            release_samples,

            ..ADSR::default()
        }
    }

    pub fn get_state(&mut self) -> CFloat {
        self.output
    }

    pub fn process(&mut self) {
        let mut output = 0.0;

        loop {
            match self.state {
                EnvelopeState::AwaitingKeyPress => {
                    if equal(self.ampl, 1.0) && not_equal(self.ampl, self.prev) {
                        self.position = 0;
                        self.state = EnvelopeState::AttackRamping;
                        println!("changed to 0:AttackRamping");
                    } else {
                        break;
                    }
                }
                EnvelopeState::AttackRamping => {
                    if self.position >= self.attack_samples {
                        self.position = 0;
                        self.state = EnvelopeState::DecayRamping;
                        println!("changed to 1:DecayRamping");
                    } else {
                        output = self.position as f64 / self.attack_samples as f64;
                        self.position += 1;
                        break;
                    }
                }
                EnvelopeState::DecayRamping => {
                    if self.position >= self.decay_samples {
                        self.position = 0;
                        self.state = EnvelopeState::Sustaining;
                        self.hold_level = self.sustain_level;
                        println!("changed to 2:Sustaining");
                    } else {
                        output = 1.0
                            - (1.0 - self.sustain_level) * self.position as f64
                                / self.decay_samples as f64;
                        self.position += 1;
                        break;
                    }
                }
                EnvelopeState::Sustaining => {
                    if equal(self.ampl, 0.0) {
                        self.position = 0;
                        self.state = EnvelopeState::ReleaseRamping;
                        println!("changed to 3:ReleaseRamping");
                    } else {
                        output = self.sustain_level;
                        break;
                    }
                }
                EnvelopeState::ReleaseRamping => {
                    if self.position >= self.release_samples {
                        self.position = 0;
                        self.state = EnvelopeState::AwaitingKeyPress;
                        println!("changed to -1:AwaitingKeyPress");
                    } else {
                        output = self.hold_level
                            - self.hold_level * self.position as f64 / self.release_samples as f64;
                        self.position += 1;
                        break;
                    }
                }
            }
        }

        if self.ampl_ramped_down() {
            self.position = 0;
            self.state = EnvelopeState::ReleaseRamping;
            self.hold_level = output;
        }

        if self.ampl_ramped_up_from_tail() {
            self.position = 0;
            self.state = EnvelopeState::AttackRamping;
        }

        self.prev = self.ampl;
        self.output = output;
    }

    pub fn next_sample(&mut self) -> CFloat {
        self.process();
        self.output
    }

    pub fn note_off(&mut self) {
        self.ampl = 0.0;
    }

    pub fn note_on(&mut self) {
        self.ampl = 1.0;
    }

    pub fn active(&self) -> bool {
        self.state != EnvelopeState::AwaitingKeyPress
    }

    pub fn modify(
        &mut self,
        attack_time: CFloat,
        decay_time: CFloat,
        sustain_level: CFloat,
        release_time: CFloat,
    ) {
        self.attack_samples  = to_samples(attack_time,  self.sample_rate);
        self.decay_samples   = to_samples(decay_time,   self.sample_rate);
        self.sustain_level   = sustain_level;
        self.release_samples = to_samples(release_time, self.sample_rate);
    }

    fn ampl_ramped_down(&self) -> bool {
        equal(self.ampl, 0.0) && not_equal(self.ampl, self.prev) && self.state != EnvelopeState::ReleaseRamping
    }

    fn ampl_ramped_up_from_tail(&self) -> bool {
        equal(self.ampl, 1.0) && not_equal(self.ampl, self.prev) && self.state == EnvelopeState::ReleaseRamping
    }
}

mod helpers {
    use super::*;

    #[inline]
    pub fn to_samples(ms: CFloat, sample_rate: CFloat) -> usize {
        (0.001 * ms * sample_rate) as usize
    }

    #[allow(dead_code)]
    #[inline]
    pub fn to_samples_f(ms: CFloat, sample_rate: CFloat) -> CFloat {
        0.001 * ms * sample_rate
    }

    #[allow(dead_code)]
    #[inline]
    pub fn to_seconds(samples: usize, sample_rate: CFloat) -> CFloat {
        samples as f64 / sample_rate
    }

    #[allow(dead_code)]
    #[inline]
    pub fn to_seconds_f(samples: CFloat, sample_rate: CFloat) -> CFloat {
        samples / sample_rate
    }
}

fn equal(first: f64, second: f64) -> bool {
    (first - second).abs() < std::f64::EPSILON
}

fn not_equal(first: f64, second: f64) -> bool {
    (first - second).abs() >= std::f64::EPSILON
}

#[cfg(test)]
mod tests {
    #![allow(clippy::float_cmp)]
    use super::helpers::*;
    use super::*;

    #[test]
    fn adsr_can_create() {
        let mut adsr = ADSR::default();

        assert_eq!(adsr.get_state(), num::zero::<f64>());
    }

    #[test]
    fn adsr_can_attack() {
        let (attack_time, decay_time, sustain_level, release_time) = default_adsr_times();
        let sample_rate = 44100.0;

        let mut adsr = ADSR::new(
            sample_rate,
            attack_time,
            decay_time,
            sustain_level,
            release_time,
        );

        assert_eq!(adsr.get_state(), 0.0);

        adsr.note_on();

        for _ in 0..=to_samples(attack_time, sample_rate) {
            adsr.process();
        }

        println!("{:#?}", adsr);

        assert_eq!(adsr.get_state(), 1.0);
    }

    #[test]
    fn adsr_can_decay() {
        let (attack_time, decay_time, sustain_level, release_time) = default_adsr_times();
        let sample_rate = 44100.0;

        let mut adsr = ADSR::new(
            sample_rate,
            attack_time,
            decay_time,
            sustain_level,
            release_time,
        );

        assert_eq!(adsr.get_state(), 0.0);

        adsr.note_on();

        for _ in 0..=to_samples(attack_time + decay_time, sample_rate) {
            adsr.process();
        }

        assert_eq!(adsr.get_state(), sustain_level);
    }

    #[test]
    fn adsr_can_sustain() {
        let (attack_time, decay_time, sustain_level, release_time) = default_adsr_times();
        let sample_rate = 44100.0;

        let mut adsr = ADSR::new(
            sample_rate,
            attack_time,
            decay_time,
            sustain_level,
            release_time,
        );

        assert_eq!(adsr.get_state(), 0.0);

        adsr.note_on();

        let lead_time = attack_time + decay_time;
        let sustain_seconds = 0.2;

        for _ in 0..=to_samples(lead_time + sustain_seconds, sample_rate) {
            adsr.process();
        }

        assert_eq!(adsr.get_state(), sustain_level);
    }

    #[test]
    fn adsr_can_release() {
        let (attack_time, decay_time, sustain_level, release_time) = default_adsr_times();
        let sample_rate = 44100.0;

        let mut adsr = ADSR::new(
            sample_rate,
            attack_time,
            decay_time,
            sustain_level,
            release_time,
        );

        assert_eq!(adsr.get_state(), 0.0);

        adsr.note_on();

        let sustain_seconds = 0.0;
        let lead_time = attack_time + decay_time + sustain_seconds;

        for _ in 0..=to_samples(lead_time, sample_rate) {
            adsr.process();
        }

        adsr.note_off();

        for _ in 0..=to_samples(release_time, sample_rate) {
            adsr.process();
        }

        assert_eq!(adsr.get_state(), 0.0);
    }

    fn default_adsr_times() -> (CFloat, CFloat, CFloat, CFloat) {
        (
            100.0, // (attack / ms)
            400.0, // (decay / ms)
            0.8,   // (sustain level)
            500.0, // (release / ms)
        )
    }
}