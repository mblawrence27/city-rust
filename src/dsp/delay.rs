use std::f64::consts::PI;
use std::iter;

use crate::utils::WrappingIndex;

pub struct Delay {
    samples: Vec<f64>,
    len: usize,
    feedback: f64,
    pub flutter: f64,
    pub flutter_speed: f64,
    flutter_state: usize,
    reel_speed: f64,
    set_reel_speed: f64,
    sr: i32,
    index: WrappingIndex,
    interpol_buffer: Vec<f64>
}

impl Delay {
    pub fn new(sr: i32, len: usize, scale: f64) -> Delay {
        let len = (len as f64 / scale) as usize;
        let samples = Delay::init_samples_buffer(len);
        let feedback = 0.0;
        let (flutter, flutter_speed, flutter_state) = Delay::get_default_flutter();
        let (set_reel_speed, reel_speed) = Delay::get_default_reel_speed();
        let index = WrappingIndex::new(0.0);

        Delay {
            samples,
            len,
            feedback,
            flutter,
            flutter_speed,
            flutter_state,
            reel_speed,
            set_reel_speed,
            sr,
            index,
            interpol_buffer: vec![0.0; 64]
        }
    }

    pub fn send_sample(&mut self, s: f64) {
        let (index, prev_index) = self.index.as_usize();
        let cc = self.len;
        let s = s + if self.feedback.is_normal() {
            self.feedback * self.return_sample()
        } else {
            0.0
        };
        let reel_speed = self.reel_speed;
        if prev_index != index {
            self.samples[(index + 1) % cc] = 0.0;
        }
        if self.flutter.is_normal() {
            let flutter_pos = self.recp_sr() * self.flutter_state as f64 * self.flutter_speed;
            let flutter_wave_sample = Delay::sinusoidal_wave(flutter_pos);
            self.reel_speed =
                self.set_reel_speed * (1.0 - self.flutter * (1.0 + flutter_wave_sample));
        }
        let sqrt_reel_speed = self.reel_speed.sqrt();
        let ss = if reel_speed > 1.0 {
            Delay::array_interpol(&mut self.interpol_buffer, self.index.current(), reel_speed, s)
        } else {
            Delay::array_interpol_simple(&mut self.interpol_buffer, self.index.current(), s)
        };
        for (i, val) in ss.iter().enumerate() {
            self.samples[(index + i) % cc] += val * sqrt_reel_speed;
        }
        self.index.wrap_increment(self.reel_speed, self.len as f64);
        Delay::wrap_increment(&mut self.flutter_state, self.sr as usize);
    }

    fn sinusoidal_wave(position: f64) -> f64 {
        (2.0 * PI * position).sin()
    }

    fn wrap_increment(value: &mut usize, wrap_len: usize) {
        *value = if *value == wrap_len - 1 {
            0
        } else {
            *value + 1
        };
    }

    pub fn recp_sr(&self) -> f64 {
        1.0 / f64::from(self.sr)
    }

    pub fn return_sample(&self) -> f64 {
        let idx = self.index.current() as usize;

        let wrapped_idx = (self.get_return_offset() + idx) % self.len;
        let next_sample = if wrapped_idx == self.len - 1 {
            self.samples[0]
        } else {
            self.samples[wrapped_idx + 1]
        };

        let rem = self.index.current() - idx as f64;
        ((1.0 - rem) * self.samples[wrapped_idx] + rem * next_sample)
    }

    pub fn get_return_offset(&self) -> usize {
        2 + self.reel_speed as usize
    }

    pub fn set_flutter_params(&mut self, flutter: f64, flutter_speed: f64) {
        self.flutter = flutter;
        self.flutter_speed = flutter_speed;
    }

    pub fn get_flutter_params(&self) -> (f64, f64) {
        (self.flutter, self.flutter_speed)
    }

    pub fn get_feedback(&self) -> f64 {
        self.feedback
    }

    pub fn set_feedback(&mut self, feedback: f64) {
        self.feedback = feedback;
    }

    pub fn set_tape_length(&mut self, seconds: f64) {
        assert!(seconds > 0.0, "tape length value must be positive");

        self.set_reel_speed = self.len as f64 / (f64::from(self.sr) * seconds);
        self.reel_speed = self.set_reel_speed;
    }

    pub fn get_buffer_len(&self) -> usize {
        self.len
    }

    pub fn array_interpol<'a>(buffer: &'a mut Vec<f64>, position: f64, speed: f64, value: f64) -> &'a [f64] {
        let speed = speed as usize;
        let rem = position - position.trunc();
        let (x1, x2) = (value * (1.0 - rem), value * rem);
        buffer[0] = x1;
        for s in buffer.iter_mut().skip(1).take(speed) {
            *s = (x1 + x2) * 0.5;
        }
        buffer[speed + 1] = x2;
        &buffer[..=speed + 1]
    }

    pub fn array_interpol_simple<'a>(buffer: &'a mut Vec<f64>, position: f64, value: f64) -> &'a [f64] {
        let rem = position - position.trunc();
        buffer[0] = value * (1.0 - rem);
        buffer[1] = value * rem;
        &buffer[..=1]
    }

    fn init_samples_buffer(len: usize) -> Vec<f64> {
        iter::repeat(0.0).take(len).collect()
    }

    fn get_default_flutter() -> (f64, f64, usize) {
        (0.0, 0.0, 0)
    }

    fn get_default_reel_speed() -> (f64, f64) {
        (1.0, 1.0)
    }

    fn get_default_params() -> (i32, usize, f64) {
        (41000, 41000, 6.0)
    }
}

impl Default for Delay {
    fn default() -> Delay {
        let (sr, len, scale) = Delay::get_default_params();
        let mut delay = Delay::new(sr, len, scale);
        delay.set_flutter_params(0.009, 9.5);
        delay
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::helpers::compare_vec_f64;

    #[test]
    fn simple_delay_can_create() {
        let (sr, len, scale) = Delay::get_default_params();

        let delay = Delay::new(sr, len, scale);

        assert_eq!(delay.get_buffer_len(), (len as f64 / scale) as usize);
        assert_eq!(delay.return_sample(), 0.0);
        assert_eq!(delay.get_flutter_params(), (0.0, 0.0));
        assert_eq!(delay.get_feedback(), 0.0);
    }

    #[test]
    fn simple_delay_can_set_flutter() {
        let (sr, len, scale) = Delay::get_default_params();
        let mut delay = Delay::new(sr, len, scale);
        let (flutter, flutter_speed) = (0.001, 6.0);

        delay.set_flutter_params(flutter, flutter_speed);

        assert_eq!(delay.get_flutter_params(), (0.001, 6.0));
    }

    #[test]
    fn simple_delay_can_set_feedback() {
        let (sr, len, scale) = Delay::get_default_params();
        let mut delay = Delay::new(sr, len, scale);
        let feedback = 0.2;

        delay.set_feedback(feedback);

        assert_eq!(delay.get_feedback(), feedback);
    }

    #[test]
    fn simple_delay_can_interpolate_with_speed() {
        let (position, speed, value) = (10.0, 1.6, 0.11);
        let scale = 1.0;
        let (sr, len, ..) = Delay::get_default_params();
        let mut delay = Delay::new(sr, len, scale);
        assert!(compare_vec_f64(
            Delay::array_interpol(&mut delay.interpol_buffer, position, speed, value),
            &vec![0.11, 0.055, 0.0]
        ));
        let (position, speed, value) = (10.25, 1.9, 0.20);
        assert!(compare_vec_f64(
            Delay::array_interpol(&mut delay.interpol_buffer, position, speed, value),
            &vec![0.15, 0.10, 0.05]
        ));
        let (position, speed, value) = (50.0, 2.5, 0.32);
        assert!(compare_vec_f64(
            Delay::array_interpol(&mut delay.interpol_buffer, position, speed, value),
            &vec![0.32, 0.16, 0.16, 0.0]
        ));
    }

    #[test]
    fn simple_delay_can_interpolate() {
        let (position, value) = (10.0, 0.11);
        let scale = 1.0;
        let (sr, len, ..) = Delay::get_default_params();
        let mut delay = Delay::new(sr, len, scale);
        assert!(compare_vec_f64(
            Delay::array_interpol_simple(&mut delay.interpol_buffer, position, value),
            &vec![0.11, 0.0]
        ));
        let (position, value) = (50.0, 0.32);
        assert!(compare_vec_f64(
            Delay::array_interpol_simple(&mut delay.interpol_buffer, position, value),
            &vec![0.32, 0.0]
        ));
    }

    #[test]
    fn simple_delay_can_return_delayed_sample() {
        let scale = 1.0;
        let (sr, len, ..) = Delay::get_default_params();
        let mut delay = Delay::new(sr, len, scale);
        let delay_len = sr as usize - delay.get_return_offset();
        let (sample_active, sample_inactive) = (0.8, 0.0);

        delay.send_sample(sample_active);
        for _ in 1..delay_len {
            assert_eq!(delay.return_sample(), sample_inactive);
            delay.send_sample(sample_inactive);
        }
        assert_eq!(delay.return_sample(), sample_active);

        delay.send_sample(sample_inactive);
        assert_eq!(delay.return_sample(), sample_inactive);
    }
}