use std::f64::consts::PI;

use crate::enum_map::Enum;
use crate::utils::WrappingIndex;

#[derive(Debug, Enum, PartialEq)]
pub enum Waveform {
    Sin,
    Saw,
    Tri,
    Bit,
    Comp,
    Pulse,
}

impl Default for Waveform {
    fn default() -> Self {
        Waveform::Sin
    }
}

pub struct Oscillator {
    wave: Box<dyn Wavefunction>,
    shape: Waveform,
    options: WaveOptions,
    index: WrappingIndex,
}

impl Oscillator {
    pub fn new(sr: f64) -> Self {
        let options = WaveOptions::new(sr);
        let shape = Default::default();
        let wave = Oscillator::switch_wave_function(&shape);
        let index = WrappingIndex::new(0.0);

        Oscillator { options, shape, wave, index }
    }

    #[inline]
    pub fn get(&self, freq: f64, amp: f64, index: f64) -> f64 {
        self.wave.get(freq, amp, index, &self.options)
    }

    #[inline]
    pub fn peek(&self, freq: f64, amp: f64) -> f64 {
        self.get(freq, amp, self.index.current())
    }

    #[inline]
    pub fn peek_offseted(&self, freq: f64, amp: f64, offset: f64) -> f64 {
        self.get(freq, amp, self.index.peek_offsetted(offset))
    }

    #[inline]
    pub fn set_mod_len(&mut self, mod_len: f64) {
        self.index.set_wrap_len_unchecked(mod_len);
    }

    #[inline]
    pub fn next(&mut self, freq: f64, amp: f64) -> f64 {
        let s = self.peek(freq, amp);
        self.incr(freq);
        s
    }

    #[inline]
    pub fn incr(&mut self, freq: f64) {
        self.index.mod_increment(1.0, self.options.sr / freq);
    }

    #[inline]
    pub fn index(&mut self) -> &mut WrappingIndex {
        &mut self.index
    }

    pub fn switch_wave(&mut self, shape: Waveform) {
        if self.shape != shape {
            self.shape = shape;
            self.wave = Oscillator::switch_wave_function(&self.shape);
        }
    }

    pub fn switch_wave_function(shape: &Waveform) -> Box<dyn Wavefunction> {
        match shape {
            Waveform::Sin   => Box::new(SinWave{}),
            Waveform::Bit   => Box::new(BitWave{}),
            Waveform::Tri   => Box::new(VinTriWave{}),
            Waveform::Saw   => Box::new(SawWave{}),
            Waveform::Comp  => Box::new(CompWave{}),
            Waveform::Pulse => Box::new(PulseWave{})
        }
    }
}

pub struct WaveOptions {
    sr: f64,
    repsr: f64,
}

impl WaveOptions {
    pub fn new(sr: f64) -> Self {
        WaveOptions { sr, repsr: 1.0 / sr }
    }
}

pub trait Wavefunction {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64;
}

pub struct SinWave {}
pub struct BitWave {}
pub struct VinTriWave {}
pub struct SawWave {}
pub struct CompWave {}
pub struct PulseWave {}

impl Wavefunction for SinWave {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64 {
        amp * (options.repsr * freq * 2.0 * PI * index).sin()
    }
}

impl Wavefunction for BitWave {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64 {
        if (options.repsr * freq * index * 2.0) as i32 % 2 == 0 {
            amp
        } else {
            -amp
        }
    }
}

impl Wavefunction for VinTriWave {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64 {
        let fl = ((1.0 - options.repsr * freq * index * 4.0) - 1.0).abs();
        fl * amp
    }
}

impl Wavefunction for SawWave {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64 {
        amp * 2.0 * (0.5 - options.repsr * freq * index)
    }
}

impl Wavefunction for CompWave {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64 {
        let fl = 2.0 * (1.0 - options.repsr * freq * index * 2.0).abs() - 1.0;
        if (options.repsr * freq * index * 2.0) as i32 % 2 == 0 {
            0.5 * amp + 0.5 * amp * fl
        } else {
            -0.5 * amp + 0.5 * amp * fl
        }
    }
}

impl Wavefunction for PulseWave {
    fn get(&self, freq: f64, amp: f64, index: f64, options: &WaveOptions) -> f64 {
        if index * freq * options.repsr > 0.7 {
            amp
        } else {
            -amp
        }
    }
}
