// broken? needs tests

use std::f64::consts::PI;

pub struct SimpleFilter { }

impl SimpleFilter {
    pub fn lpf(input: f64, previous_input: f64, fc: f64, sr: f64) -> f64 {
        let dt = 1.0 / sr;
        let rc = 1.0 / (fc * 2.0 * PI);
        let a = dt / (rc + dt);
        previous_input + a * (input - previous_input)
    }
    pub fn hpf(input: f64, previous_input: f64, previous_output: f64, fc: f64, sr: f64) -> f64 {
        let dt = 1.0 / sr;
        let rc = 1.0 / (fc * 2.0 * PI);
        let a = rc / (rc + dt);
        a * (input + previous_output - previous_input)
    }
}

#[derive(Default)]
pub struct SimpleHighPass {
    previous_input: f64,
    previous_output: f64,
    sr: Option<f64>,
}

impl SimpleHighPass {
    pub fn new(sr: f64) -> Self {
        SimpleHighPass {
            sr: Some(sr),

            ..Default::default()
        }
    }

    pub fn process(&mut self, input: f64, fc: f64) -> f64 {
        let output = match self.sr {
            Some(sr) if fc > 0.0 => SimpleFilter::hpf(input, self.previous_input, self.previous_output, fc, sr),
            _ => input
        };
        self.previous_input = input;
        self.previous_output = output;

        output
    }
}


#[derive(Default)]
pub struct SimpleLowPass {
    previous_input: f64,
    sr: Option<f64>,
}

impl SimpleLowPass {
    pub fn new(sr: f64) -> Self {
        SimpleLowPass {
            sr: Some(sr),

            ..Default::default()
        }
    }

    pub fn process(&mut self, input: f64, fc: f64) -> f64 {
        let output = if let Some(sr) = self.sr {
            SimpleFilter::lpf(input, self.previous_input, fc, sr)
        } else {
            0.0
        };
        self.previous_input = input;

        output
    }
}