#[macro_use]
pub extern crate enum_map;
pub extern crate num;
pub extern crate num_traits;
pub extern crate rand;
pub extern crate rand_xorshift;
pub extern crate rand_core;

pub mod dsp;
pub mod math;
pub mod synth;
pub mod utils;

pub mod tests {
    pub mod helpers {
        pub fn compare_f64(observed: f64, expected: f64) -> bool {
            let tolerance: f64 = 1.0e-10;
            let abs_expected = expected.abs();
            if abs_expected == 0.0 {
                return observed.abs() == 0.0;
            }
            let chi_sq = (observed - expected) * (observed - expected) / abs_expected;
            println!(
                "chi_sq = {:+13.5e}, dp = {}",
                chi_sq,
                chi_sq.log10().round()
            );
            chi_sq <= tolerance
        }

        pub fn compare_vec_f64(observed: &[f64], expected: &[f64]) -> bool {
            assert_eq!(observed.len(), expected.len());
            observed
                .iter()
                .zip(expected)
                .all(|(o, e)| compare_f64(*o, *e))
        }
    }
}