use crate::num::complex::Complex64;
use crate::num::{One, Zero};

use std::ops::{Mul, MulAssign, Rem};
use std::{fmt, iter};

#[derive(Debug, PartialEq)]
pub struct PolynomialResult(Vec<Complex64>);

impl From<&Vec<Complex64>> for PolynomialResult {
    fn from(vec: &Vec<Complex64>) -> PolynomialResult {
        PolynomialResult(vec.clone())
    }
}

impl PolynomialResult {
    pub fn re(&self) -> Vec<f64> {
        self.0.iter().map(|c| c.re).collect::<Vec<f64>>()
    }
}

#[derive(Debug)]
pub struct PolynomialMultiplier {
    display_var: char,
    start_index: i32,
    state: PolynomialResult,
}

impl PolynomialMultiplier {
    pub fn new(order: Option<usize>, start_index: Option<i32>) -> PolynomialMultiplier {
        let state = match order {
            Some(_order) => PolynomialMultiplier::init_result(_order),
            None => PolynomialMultiplier::init_result(1),
        };
        let start_index = match start_index {
            Some(i) => i,
            None => 0,
        };
        PolynomialMultiplier {
            display_var: 'z',
            start_index,
            state,
        }
    }

    pub fn result(&self) -> &PolynomialResult {
        &self.state
    }

    pub fn multiply(&mut self, rhs: &PolynomialResult) {
        let (this, other) = (&self.state.0, &rhs.0);
        let (bigger, smaller) = if this.len() > other.len() {
            (this, other)
        } else {
            (other, this)
        };

        assert!(!smaller.is_empty());

        let padded = PolynomialMultiplier::pad_vec(&bigger, smaller.len() - 1, Complex64::zero());
        let mut smaller = smaller.clone();
        smaller.reverse();
        let conv_result = padded
            .windows(smaller.len())
            .map(|window| window.iter().zip(&smaller).map(|(a, b)| a * b).sum())
            .skip_while(Zero::is_zero)
            .collect();

        self.state = PolynomialResult(conv_result);
    }

    fn pad_vec<T>(vec: &[T], pad_count: usize, pad_value: T) -> Vec<T>
    where
        T: std::clone::Clone,
    {
        let zero_gen = iter::repeat(pad_value).take(pad_count).collect::<Vec<T>>();
        let mut padded = vec![];

        padded.extend_from_slice(&zero_gen[..]);
        padded.extend_from_slice(&vec[..]);
        padded.extend_from_slice(&zero_gen[..]);
        padded
    }

    pub fn multiply_by(&mut self, rhs: f64) {
        for i in 0..self.state.0.len() {
            self.state.0[i] *= rhs;
        }
    }

    fn init_result(order: usize) -> PolynomialResult {
        assert!(order > 0);

        let mut vec = vec![Complex64::one()];

        for _ in 1..order {
            vec.push(Complex64::zero());
        }

        PolynomialResult(vec)
    }

    pub fn order(&self) -> usize {
        self.state
            .0
            .iter()
            .skip_while(|coef| coef == &&Complex64::zero())
            .count()
    }

    pub fn set_start_index(&mut self, index: i32) {
        self.start_index = index;
    }
}

impl From<Vec<Complex64>> for PolynomialMultiplier {
    fn from(vec: Vec<Complex64>) -> PolynomialMultiplier {
        PolynomialMultiplier {
            display_var: 'x',
            start_index: 0,
            state: PolynomialResult(vec),
        }
    }
}

impl From<PolynomialResult> for PolynomialMultiplier {
    fn from(res: PolynomialResult) -> PolynomialMultiplier {
        PolynomialMultiplier {
            display_var: 'x',
            start_index: 0,
            state: res,
        }
    }
}

impl Mul<PolynomialResult> for PolynomialMultiplier {
    type Output = PolynomialMultiplier;
    fn mul(mut self, rhs: PolynomialResult) -> Self::Output {
        self.multiply(&rhs);
        self
    }
}

impl MulAssign<&PolynomialResult> for PolynomialMultiplier {
    fn mul_assign(&mut self, rhs: &PolynomialResult) {
        self.multiply(rhs);
    }
}

impl fmt::Display for PolynomialMultiplier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        let max_power = self.start_index + (self.order() as i32) - 1;
        let fmt_str = self
            .state
            .0
            .iter()
            .filter(|coef| !coef.is_zero())
            .enumerate()
            .map(|(idx, coef)| {
                if coef.im == 0.0 {
                    if coef.re.rem(1.0) == 0.0 {
                        format!("{}{}^{}", coef.re, self.display_var, max_power - idx as i32)
                    } else {
                        format!(
                            "{:.3e}{}^{}",
                            coef.re,
                            self.display_var,
                            max_power - idx as i32
                        )
                    }
                } else if coef.re == 0.0 {
                    if coef.im.rem(1.0) == 0.0 {
                        format!(
                            "({}i){}^{}",
                            coef.im,
                            self.display_var,
                            max_power - idx as i32
                        )
                    } else {
                        format!(
                            "({:.3e}i){}^{}",
                            coef.im,
                            self.display_var,
                            max_power - idx as i32
                        )
                    }
                } else if coef.re.rem(1.0) == 0.0 && coef.im.rem(1.0) == 0.0 {
                    format!(
                        "({}+{}i){}^{}",
                        coef.re,
                        coef.im,
                        self.display_var,
                        max_power - idx as i32
                    )
                } else {
                    format!(
                        "({:.3e}{:+.3e}i){}^{}",
                        coef.re,
                        coef.im,
                        self.display_var,
                        max_power - idx as i32
                    )
                }
            })
            .collect::<Vec<_>>()
            .join(" + ");
        write!(f, "{}", fmt_str)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn polyn_mltply_can_create() {
        let mult = PolynomialMultiplier::new(None, None);

        assert_eq!(mult.result(), &PolynomialResult(vec![Complex64::one()]));
    }

    #[test]
    fn polyn_mltply_can_convert() {
        let converted = PolynomialMultiplier::from(zero_order_result(0.0));

        assert_eq!(
            converted.result(),
            &PolynomialResult(vec![Complex64::zero()])
        );
    }

    #[test]
    fn polyn_mltply_can_mult_two_zero_orders() {
        let polymul = PolynomialMultiplier::from(zero_order_result(5.0));
        let constant = zero_order_result(2.0);

        let product = polymul * constant;

        assert_eq!(product.result(), &zero_order_result(10.0));
    }

    #[test]
    fn polyn_mltply_can_scale_polyn_by_zero_order() {
        let polymul = PolynomialMultiplier::from(second_order_result(1.0, 2.0, 3.0));
        let constant = zero_order_result(2.0);

        let product = polymul * constant;

        assert_eq!(product.result(), &second_order_result(2.0, 4.0, 6.0));
    }

    #[test]
    fn polyn_mltply_can_scale_by_constant() {
        let mut polymul = PolynomialMultiplier::from(zero_order_result(5.0));
        let constant = 3.0;

        polymul.multiply_by(constant);

        assert_eq!(polymul.result(), &zero_order_result(15.0));
    }

    #[test]
    fn polyn_mltply_can_mult_polys() {
        fn multiply(first: (f64, f64), second: (f64, f64)) -> PolynomialResult {
            let polymul = PolynomialMultiplier::from(first_order_result(first.0, first.1));
            let rhs = first_order_result(second.0, second.1);

            let product = polymul * rhs;

            PolynomialResult(product.result().0.clone())
        }

        assert_eq!(
            multiply((1.0, 4.0), (1.0, 2.0)),
            second_order_result(1.0, 6.0, 8.0)
        );
        assert_eq!(
            multiply((2.0, 0.0), (1.0, 2.0)),
            second_order_result(2.0, 4.0, 0.0)
        );
        assert_eq!(
            multiply((1.0, 1.0), (1.0, 1.0)),
            second_order_result(1.0, 2.0, 1.0)
        );
    }

    #[test]
    fn polyn_mltply_can_mult_polys_complex() {
        fn multiply(
            first: ((f64, f64), (f64, f64)),
            second: ((f64, f64), (f64, f64)),
        ) -> PolynomialResult {
            let polymul = PolynomialMultiplier::from(first_order_result_complex(first.0, first.1));
            let rhs = first_order_result_complex(second.0, second.1);

            let product = polymul * rhs;

            PolynomialResult(product.result().0.clone())
        }

        assert_eq!(
            multiply(((1.0, 0.0), (1.0, 0.0)), ((1.0, 0.0), (1.0, 0.0))),
            second_order_complex_result((1.0, 0.0), (2.0, 0.0), (1.0, 0.0))
        );
        assert_eq!(
            multiply(((1.0, 0.0), (0.0, -1.0)), ((1.0, 0.0), (0.0, 1.0))),
            second_order_complex_result((1.0, 0.0), (0.0, 0.0), (1.0, 0.0))
        );
    }

    #[test]
    fn polyn_mltply_can_mult_polys_mutably() {
        let mut polymul = PolynomialMultiplier::from(first_order_result(1.0, 4.0));
        let other = first_order_result(1.0, 2.0);

        polymul *= &other;

        assert_eq!(polymul.result(), &second_order_result(1.0, 6.0, 8.0));
    }

    #[test]
    fn polyn_mltply_can_print_polys() {
        let coofs = (1.0, 6.0, 8.0);

        let poly = PolynomialMultiplier::from(second_order_result(coofs.0, coofs.1, coofs.2));

        assert_eq!(format!("{}", poly), "1x^2 + 6x^1 + 8x^0");
    }

    #[test]
    fn polyn_mltply_can_print_polys_complex() {
        let coofs = ((1.0, 4.0), (0.0, 2.0), (8.0, 0.0));

        let poly =
            PolynomialMultiplier::from(second_order_complex_result(coofs.0, coofs.1, coofs.2));

        assert_eq!(format!("{}", poly), "(1+4i)x^2 + (2i)x^1 + 8x^0");
    }

    #[test]
    fn polyn_mltply_can_print_polys_offset() {
        let coofs = (1.0, 6.0, 8.0);

        let mut poly = PolynomialMultiplier::from(second_order_result(coofs.0, coofs.1, coofs.2));
        poly.set_start_index(-2);

        assert_eq!(format!("{}", poly), "1x^0 + 6x^-1 + 8x^-2");
    }

    fn zero_order_result(x0: f64) -> PolynomialResult {
        PolynomialResult(vec![Complex64::from(x0)])
    }

    fn first_order_result(x1: f64, x0: f64) -> PolynomialResult {
        PolynomialResult(vec![Complex64::from(x1), Complex64::from(x0)])
    }

    fn first_order_result_complex(x1: (f64, f64), x0: (f64, f64)) -> PolynomialResult {
        PolynomialResult(vec![Complex64::new(x1.0, x1.1), Complex64::new(x0.0, x0.1)])
    }

    fn second_order_result(x2: f64, x1: f64, x0: f64) -> PolynomialResult {
        PolynomialResult(vec![
            Complex64::from(x2),
            Complex64::from(x1),
            Complex64::from(x0),
        ])
    }

    fn second_order_complex_result(
        x2: (f64, f64),
        x1: (f64, f64),
        x0: (f64, f64),
    ) -> PolynomialResult {
        PolynomialResult(vec![
            Complex64::new(x2.0, x2.1),
            Complex64::new(x1.0, x1.1),
            Complex64::new(x0.0, x0.1),
        ])
    }
}