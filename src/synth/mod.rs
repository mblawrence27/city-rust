pub mod manager;
pub mod poly;
pub mod sender;
pub mod voice;

const VERBOSE_PRINT: bool = false;

mod keys {
    const FIRST_KB_NOTE_ST_OFFSET: f64 = 3.0;

    pub fn st_from_key_code(key_code: u8) -> Option<f64> {
        if let Some(key) = to_key(key_code) {
            let offset = match key {
                Keys::Q            => Some(-1.0),
                Keys::A            => Some(0.0),
                Keys::W            => Some(1.0),
                Keys::S            => Some(2.0),
                Keys::E            => Some(3.0),
                Keys::D            => Some(4.0),
                Keys::F            => Some(5.0),
                Keys::T            => Some(6.0),
                Keys::G            => Some(7.0),
                Keys::Y            => Some(8.0),
                Keys::H            => Some(9.0),
                Keys::U            => Some(10.0),
                Keys::J            => Some(11.0),
                Keys::K            => Some(12.0),
                Keys::O            => Some(13.0),
                Keys::L            => Some(14.0),
                Keys::P            => Some(15.0),
                Keys::OemSemicolon => Some(16.0),
                Keys::Oemtilde     => Some(17.0),
                _                  => None,
            };
            offset.map(|st| st + FIRST_KB_NOTE_ST_OFFSET)
        } else {
            None
        }
    }

    pub fn to_key(key_code: u8) -> Option<Keys> {
        match key_code {
            65  => Some(Keys::A),
            66  => Some(Keys::B),
            67  => Some(Keys::C),
            68  => Some(Keys::D),
            69  => Some(Keys::E),
            70  => Some(Keys::F),
            71  => Some(Keys::G),
            72  => Some(Keys::H),
            73  => Some(Keys::I),
            74  => Some(Keys::J),
            75  => Some(Keys::K),
            76  => Some(Keys::L),
            77  => Some(Keys::M),
            78  => Some(Keys::N),
            79  => Some(Keys::O),
            80  => Some(Keys::P),
            81  => Some(Keys::Q),
            82  => Some(Keys::R),
            83  => Some(Keys::S),
            84  => Some(Keys::T),
            85  => Some(Keys::U),
            86  => Some(Keys::V),
            87  => Some(Keys::W),
            88  => Some(Keys::X),
            89  => Some(Keys::Y),
            90  => Some(Keys::Z),
            186 => Some(Keys::OemSemicolon),
            187 => Some(Keys::Oemplus),
            188 => Some(Keys::Oemcomma),
            189 => Some(Keys::OemMinus),
            192 => Some(Keys::Oemtilde),
            _ => None
        }
    }

    pub fn semitones_to_factor(semitone_offset: Option<f64>) -> f64 {
        let oct = semitone_offset.or(Some(0.0)).unwrap() / 12.0;
        2.0_f64.powf(oct)
    }
    
    pub enum Keys {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        OemSemicolon,
        Oemplus,
        Oemcomma,
        OemMinus,
        Oemtilde,
    }
}