use std::cell::RefCell;
use std::rc::Rc;
use std::iter;

use crate::dsp::delay::Delay;
use crate::dsp::reverb::Reverb;
use crate::utils::loader;
use crate::synth::sender::{KeyData, KeyEventHandler, MidiData, NoteEvent, StateDispatcher};
use crate::synth::manager::VoiceScheduler;
use crate::synth::voice::SynthVoice;
use crate::utils::parameters::R;
use crate::utils::preset::Preset;
use crate::utils::queue::MessageQueue;
use crate::utils::state::StateManager;

pub struct CitySynth {
    voices: VoiceScheduler,
    reverb: Reverb,
    delay: Delay,
    buffer: Vec<f64>,
    dispatcher: StateDispatcher,
    pub reverb_wet: f64,
    pub delay_wet: f64,
    pub dverb_active: bool,
    active: bool
}

impl CitySynth {
    pub fn new<K>(sr: f64, voices_count: usize, bucket: StateManager<R, f64>) -> (Self, KeyEventHandler<K>)
        where K: NoteEvent
    {
        let voice_factory = {
            let bucket = bucket.clone();
            move |i| {
                let mut voice = SynthVoice::new(sr, i, bucket.clone());
                voice.set_unison_controlled(false);
                voice
            }
        };
        let queue = MessageQueue::default();
        let key_events = KeyEventHandler::new(queue.dispatch(), voices_count);
        let dispatcher = StateDispatcher::new(bucket, queue.dispatch());
        let voices = VoiceScheduler::new(Rc::new(RefCell::new(queue)), Box::new(voice_factory), voices_count);

        let nested_lens = Reverb::generate_delay_lens(6, sr, 3.0, 3.0);
        let reverb = Reverb::new(sr as i32, nested_lens);
        let mut delay = Delay::new(sr as i32, sr as usize, 6.0);
        delay.set_flutter_params(0.009, 9.5);
        delay.set_tape_length(0.5);
        dispatcher.dispatch().unwrap();

        let synth = CitySynth {
            voices,
            reverb,
            delay,
            buffer: vec![],
            dispatcher,
            reverb_wet: 0.23,
            delay_wet: 0.5,
            active: false,
            dverb_active: true
        };

        (synth, key_events)
    }
    
    pub fn read(&mut self, buffer: &mut [f32]) {
        let cc = buffer.len();
        for s in self.buffer.iter_mut() { *s = 0.0 }

        if self.buffer.len() != cc {
            self.buffer.resize(cc, 0.0);
        }

        self.voices.process_updates();

        for voice in self.voices.voices_mut() {
            voice.read(&mut self.buffer);
        }

        let voices_active =  self.voices.has_active_voices();
        if voices_active {
            self.active = true;
        }

        let delay_wet = self.dispatcher.get(R::DelayWet);
        let reverb_wet = self.dispatcher.get(R::ReverbWet);

        for (idx, s) in self.buffer.iter().enumerate() {
            let mut s = *s;
            if self.dverb_active && self.active {
                if delay_wet > 0.0 {
                    let delayed = self.delay.return_sample() * delay_wet;
                    s = delayed + s * (1.0 - delay_wet);
                }
                if reverb_wet > 0.0 {
                    s = self.reverb.get_sample(s, reverb_wet);
                }
                if delay_wet > 0.0 {
                    self.delay.send_sample(s);
                }
            }
            buffer[idx] = s as f32;
        }
    }

    pub fn set_buffer_len(&mut self, samples: usize) {
        if self.buffer.len() != samples {
            self.buffer = iter::repeat(0.0).take(samples).collect();
        }
    }

    pub fn state(&self) -> &StateDispatcher {
        &self.dispatcher
    }

    pub fn state_mut(&mut self) -> &mut StateDispatcher {
        &mut self.dispatcher
    }

    pub fn refresh(&mut self) {
        for voice in self.voices.voices_mut() {
            voice.refresh();
        }
    }

    pub fn save_preset(&self, preset_name: String) -> Result<Preset, ()> {
        loader::save_preset(self.dispatcher.inner(), preset_name)
    }
    
    pub fn load_preset(&mut self, preset_data: String) -> Result<(), String> {
        let preset = Preset::process_line(&preset_data)
            .map_err(|err| format!("{}", err))?;

        loader::load_defaults(self.dispatcher.inner_mut());

        loader::load_commands(self.dispatcher.inner_mut(), &preset.commands)
            .map_err(|cmds: Vec<_>| format!("had problems loading these commands:\n{:#?}", cmds))?;

        self.dispatcher.dispatch()
            .map_err(|_| String::from("could not send updates to voice manager"))?;

        Ok(self.refresh())
    }

    pub fn note_on(key_events: &mut KeyEventHandler<KeyData>, key_code: u8, oct: i32) {
        key_events.key_down(KeyData::new(key_code, oct));
        key_events.fire_events();
    }

    pub fn note_off(key_events: &mut KeyEventHandler<KeyData>, key_code: u8) {
        key_events.key_up(KeyData::from(key_code));
        key_events.fire_events();
    }

    pub fn midi_note_on(key_events: &mut KeyEventHandler<MidiData>, midi_note: u8, vel: u8) {
        key_events.key_down(MidiData::new(midi_note, vel));
        key_events.fire_events();
    }

    pub fn midi_note_off(key_events: &mut KeyEventHandler<MidiData>, midi_note: u8) {
        key_events.key_up(MidiData::from(midi_note));
        key_events.fire_events();
    }
}

#[cfg(test)]
mod tests {
    const BUFFER_SIZE: usize = 2048;

    use super::*;
    use crate::utils::loader;

    fn init_poly<K>(voices_count: usize) -> (CitySynth, KeyEventHandler<K>) where K: NoteEvent {
        let sr = 44100.0;
        let mut manager = StateManager::new();
        loader::load_defaults(&mut manager);

        CitySynth::new(sr, voices_count, manager)
    }

    #[test]
    fn poly_synth_can_create() {
        let _poly: (_, KeyEventHandler<KeyData>) = init_poly(1);
    }

    #[test]
    fn poly_synth_can_create_midi() {
        let _poly: (_, KeyEventHandler<MidiData>) = init_poly(1);
    }

    #[test]
    fn poly_synth_can_save_shallow_params() {
        let (mut synth, ..): (_, KeyEventHandler<MidiData>) = init_poly(1);

        test_shallow_key(&mut synth, R::DelayWet);
        test_shallow_key(&mut synth, R::ReverbWet);
    }

    fn test_shallow_key(synth: &mut CitySynth, key: R) {
        use crate::utils::state::PresetState;
        let mapping = &R::mapping()[key];

        let command_iden = mapping.command_iden.unwrap()
            .to_owned();

        let low_value = 0.00;
        synth.dispatcher.set(key, low_value);
        let first = synth.save_preset(String::from("low-value")).unwrap();

        let first = first.commands.iter()
            .find(|cmd| cmd.opcode == command_iden)
            .unwrap();

        assert!((first.value.unwrap() - low_value as f32).abs() < std::f32::EPSILON);

        let high_value = 0.99;
        synth.dispatcher.set(key, high_value);
        let second = synth.save_preset(String::from("high-value")).unwrap();

        let second = second.commands.iter()
            .find(|cmd| cmd.opcode == command_iden)
            .unwrap();

        assert!((second.value.unwrap() - high_value as f32).abs() < std::f32::EPSILON);
    }

    #[test]
    fn poly_synth_can_play_mono() {
        let mut buffer = [0.0; BUFFER_SIZE];
        let (mut synth, mut key_events) = init_poly(1);
        play_two_notes(&mut synth, &mut key_events, &mut buffer[..]);
    }

    #[test]
    fn poly_synth_can_play_poly() {
        let mut buffer = [0.0; BUFFER_SIZE];
        let (mut synth, mut key_events) = init_poly(4);
        shorten_synth_response_times(&mut synth);
        
        play_two_notes(&mut synth, &mut key_events, &mut buffer[..]);
    }

    #[test]
    fn poly_synth_can_play_poly_midi() {
        let mut buffer = [0.0; BUFFER_SIZE];
        let (mut synth, mut key_events) = init_poly(2);
        shorten_synth_response_times(&mut synth);
        
        play_two_midi_notes(&mut synth, &mut key_events, &mut buffer[..]);
    }

    fn play_two_notes(synth: &mut CitySynth, key_events: &mut KeyEventHandler<KeyData>, buffer: &mut[f32]) {
        let (note_a_key_code, note_c_key_code) = (72, 65);
        
        let rms_before = read_into_buffer(synth, buffer);
        assert!(rms_before < std::f32::EPSILON);

        println!("note ON [A]");
        CitySynth::note_on(key_events, note_a_key_code, 0);
        read_into_buffer(synth, buffer);

        println!("note ON [C]");
        CitySynth::note_on(key_events, note_c_key_code, 0);
        let summed: f32 = (0..8).map(|_| read_into_buffer(synth, buffer)).sum();
        assert!(summed > 0.0);

        synth.state_mut().set(R::LPFmodrate, 101010.0);

        println!("note OFF [C]");
        CitySynth::note_off(key_events, note_c_key_code);
        read_into_buffer(synth, buffer);

        println!("note OFF [A]");
        CitySynth::note_off(key_events, note_a_key_code);
        for _ in 0..8 { read_into_buffer(synth, buffer); }
    }

    fn play_two_midi_notes(synth: &mut CitySynth, key_events: &mut KeyEventHandler<MidiData>, buffer: &mut[f32]) {
        let (note_c_midi_code, note_d_midi_code) = (60, 61);
        
        let rms_before = read_into_buffer(synth, buffer);
        assert!(rms_before < std::f32::EPSILON);

        println!("note ON [D]");
        CitySynth::midi_note_on(key_events, note_d_midi_code, 0);
        read_into_buffer(synth, buffer);

        println!("note ON [C]");
        CitySynth::midi_note_on(key_events, note_c_midi_code, 0);
        let summed: f32 = (0..8).map(|_| read_into_buffer(synth, buffer)).sum();
        assert!(summed > 0.0);

        synth.state_mut().set(R::LPFmodrate, 101010.0);

        println!("note OFF [C]");
        CitySynth::midi_note_off(key_events, note_c_midi_code);
        read_into_buffer(synth, buffer);

        println!("note OFF [D]");
        CitySynth::midi_note_off(key_events, note_d_midi_code);
        for _ in 0..8 { read_into_buffer(synth, buffer); }
    }

    fn shorten_synth_response_times(synth: &mut CitySynth) {
        synth.delay_wet = 0.0;
        synth.reverb_wet = 0.0;
        synth.state_mut().set(R::Attack, 0.001);
        synth.state_mut().set(R::Decay, 0.001);
        synth.state_mut().set(R::Sustain, 1.0);
        synth.state_mut().set(R::Release, 0.001);
        synth.refresh();
    }

    fn read_into_buffer(synth: &mut CitySynth, buffer: &mut[f32]) -> f32 {
        synth.read(&mut buffer[..]);
        let rms = (buffer.iter().map(|s| s * s).sum::<f32>() / BUFFER_SIZE as f32).sqrt();
        dbg!(rms);
        rms
    }
}
