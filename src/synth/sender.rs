use std::collections::HashSet;
use std::cmp;
use std::fmt::Debug;
use std::hash::Hash;
use std::iter::Iterator;

use crate::utils::parameters::R;
use crate::utils::queue::Dispatcher;
use crate::utils::state::{Bucket, StateManager};

use super::VERBOSE_PRINT;

#[derive(Clone, Debug)]
pub enum Message {
    NoteOn(KeyData),
    NoteOff(KeyData),
    MidiNoteOn(MidiData),
    MidiNoteOff(MidiData),
    PitchBend(u8, f64),
    StateFill(ClonedBucket),
    StateUpdate((R, f64)),
    NoOp
}

pub trait NoteEvent : Clone + Copy + Debug + Eq + Hash {
    fn to_note_on(self) -> Message;
    fn to_note_off(self) -> Message;
}

#[derive(Clone, Copy, Debug, Eq, Hash)]
pub struct KeyData {
    pub key_code: u8,
    pub octave: Option<i32>,
}

impl KeyData {
    pub fn new(key_code: u8, octave: i32) -> Self {
        Self {
            key_code,
            octave: Some(octave)
        }
    }

    pub fn from(key_code: u8) -> Self {
        Self {
            key_code,
            octave: None
        }
    }
}

impl NoteEvent for KeyData {
    fn to_note_on(self)  -> Message { Message::NoteOn(self)  }
    fn to_note_off(self) -> Message { Message::NoteOff(self) }
}

impl PartialEq for KeyData {
    fn eq(&self, other: &Self) -> bool {
        self.key_code == other.key_code
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash)]
pub struct MidiData {
    pub midi_note: u8,
    pub velocity: Option<u8>,
}

impl MidiData {
    pub fn new(midi_note: u8, velocity: u8) -> Self {
        Self {
            midi_note,
            velocity: Some(velocity)
        }
    }

    pub fn from(midi_note: u8) -> Self {
        Self {
            midi_note,
            velocity: None
        }
    }
}

impl NoteEvent for MidiData {
    fn to_note_on(self)  -> Message { Message::MidiNoteOn(self)  }
    fn to_note_off(self) -> Message { Message::MidiNoteOff(self) }
}

impl PartialEq for MidiData {
    fn eq(&self, other: &Self) -> bool {
        self.midi_note == other.midi_note
    }
}

#[derive(Debug)]
pub struct ClonedBucket {
    pub inner: Box<Bucket<R, f64>>
}

impl Clone for ClonedBucket {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone()
        }
    }
}

pub struct KeyEventHandler<K> where K: NoteEvent {
    msg_tx: Dispatcher<Message>,
    key_stack: Vec<K>,
    bounded_set: HashSet<K>,
    max_count: usize
}

impl<K> KeyEventHandler<K> where K: NoteEvent {
    pub fn new(dispatcher: Dispatcher<Message>, max_count: usize) -> Self {
        Self {
            msg_tx: dispatcher,
            key_stack: Vec::with_capacity(max_count),
            bounded_set: HashSet::with_capacity(max_count),
            max_count
        }
    }

    pub fn key_down(&mut self, key: K) {
        let elem = self.key_stack.iter()
            .enumerate()
            .find(|(_, elem)| elem == &&key);

        if let Some((idx, _)) = elem {
            self.key_stack.remove(idx);
        }
        self.key_stack.push(key);
    }

    pub fn key_up(&mut self, key: K) {
        let mut iter = self.key_stack.iter()
            .enumerate();
        let elem = iter.find(|(_, &elem)| elem == key);
        if let Some((idx, _)) = elem {
            if iter.next().is_none() {
                self.key_stack.pop();
            } else {
                self.key_stack.remove(idx);
            }
        }
    }

    pub fn fire_events(&mut self) {
        let start_idx = cmp::max(0, self.key_stack.len() as isize - self.max_count as isize);

        let new_set = self.key_stack[start_idx as usize..]
            .iter()
            .copied()
            .collect::<HashSet<_>>();

        let sustained_set = new_set.intersection(&self.bounded_set)
            .copied()
            .collect();
        let released_set = self.bounded_set.difference(&sustained_set);
        let pressed_set = new_set.difference(&sustained_set);

        for &elem in released_set {
            if VERBOSE_PRINT {
                println!("key released -> note off sent: {:?}", elem);
            }
            self.msg_tx.send(elem.to_note_off()).unwrap();
        }
        for &elem in pressed_set {
           if VERBOSE_PRINT {
                println!("key pressed -> note on sent: {:?}", elem);
           }
            self.msg_tx.send(elem.to_note_on()).unwrap();
        }
        self.bounded_set = new_set;
    }
}


pub struct StateDispatcher {
    bucket: StateManager<R, f64>,
    msg_tx: Dispatcher<Message>,
}

impl StateDispatcher {
    pub fn new(bucket: StateManager<R, f64>, dispatcher: Dispatcher<Message>) -> Self {
        Self {
            bucket,
            msg_tx: dispatcher
        }
    }

    pub fn inner(&self) -> &StateManager<R, f64> {
        &self.bucket
    }

    pub fn inner_mut(&mut self) -> &mut StateManager<R, f64> {
        &mut self.bucket
    }

    pub fn dispatch(&self) -> Result<(), ()> {
        let cloned = self.bucket.inner_clone();
        let cloned = ClonedBucket { inner: Box::new(cloned) };

        self.msg_tx.send(Message::StateFill(cloned))
    }

    pub fn get(&self, key: R) -> f64 {
        self.bucket.get(key)
    }

    pub fn set(&mut self, key: R, value: f64) {
        self.bucket.set(key, value);

        self.msg_tx.send(Message::StateUpdate((key, value))).expect("couldn't dispatch state update");
    }
}
