use std::cmp::min;
use std::hash::{Hash, Hasher};

use crate::dsp::adsr::ADSR;
use crate::dsp::butterworth::ButterworthFilter;
use crate::dsp::oscillator::Oscillator;
use crate::dsp::simple::rc::SimpleHighPass;
use crate::utils::state::{StateManager, StateKeys};
use crate::utils::parameters::R;

const CONST_A4_FREQ: f64 = 440.0;
const CONST_AMPL_FACT: f64 = 0.9;
const CONST_AFF_VALUE: f64 = 0.7;
const CONST_LPF_DUCK_FREQ: f64 = 100.0;
const CONST_LPF_DUCK_ADSR: (f64, f64, f64, f64) = (50.0, 0.0, 1.0, 250.0);
const CONST_MAX_VEL: f64 = 127.0;

pub struct SynthVoice {
    osc_fundamental_nco: Oscillator,
    osc_harmonics_nco: Oscillator,
    osc_subosc_nco: Oscillator,
    osc_filter_mod: Oscillator,
    osc_freq_mod: Oscillator,
    osc_ampl_mod: Oscillator,

    filter_hpf: SimpleHighPass,
    filter_lpf: ButterworthFilter,

    adsr_ampl: ADSR,
    adsr_filter_duck: ADSR,
    adsr_filter_mod: ADSR,

    is_unison_controlled: bool,
    iden: usize,

    bucket: StateManager<R, f64>,

    ampl_fact: f64,
    freq: f64,
    ff: f64,
    lpf: f64,
    sr: f64,
    vel: f64,
}

impl SynthVoice {
    pub fn new(sr: f64, iden: usize, bucket: StateManager<R, f64>) -> Self {
        let b = &bucket;

        let osc_fundamental_nco = Oscillator::new(sr);
        let osc_harmonics_nco = Oscillator::new(sr);
        let osc_subosc_nco = Oscillator::new(sr);
        let osc_filter_mod = Oscillator::new(sr);
        let osc_freq_mod = Oscillator::new(sr);
        let osc_ampl_mod = Oscillator::new(sr);

        let filter_hpf = SimpleHighPass::new(sr);
        let filter_lpf = ButterworthFilter::new(4, sr);

        let (a, d, s, r) = (R::Attack.get(b), R::Decay.get(b), R::Sustain.get(b), R::Release.get(b));
        let adsr_ampl = ADSR::new(sr, a, d, s, r);

        let (a, d, s, r) = CONST_LPF_DUCK_ADSR;
        let adsr_filter_duck = ADSR::new(sr, a, d, s, r);

        let (a, d, s, r) = (R::LPFattack.get(b), 0.0, 1.0, R::LPFrelease.get(b));
        let adsr_filter_mod = ADSR::new(sr, a, d, s, r);

        let lpf = R::LPF.get(b);

        SynthVoice {
            osc_fundamental_nco,
            osc_harmonics_nco,
            osc_subosc_nco,
            osc_filter_mod,
            osc_freq_mod,
            osc_ampl_mod,
            filter_hpf,
            filter_lpf,
            adsr_ampl,
            adsr_filter_duck,
            adsr_filter_mod,
            is_unison_controlled: true,
            iden,
            bucket,
            ampl_fact: CONST_AMPL_FACT,
            freq: CONST_A4_FREQ,
            ff: CONST_A4_FREQ,
            lpf,
            sr,
            vel: CONST_MAX_VEL,
        }
    }

    pub fn set_unison_controlled(&mut self, is_unison_voice: bool) {
        self.is_unison_controlled = is_unison_voice;
    }

    pub fn set_freq(&mut self, freq: f64) {
        self.freq = freq;
        self.ff = freq;
    }

    pub fn set_freq_now(&mut self, freq: f64) {
        self.ff = freq;
    }

    pub fn freq(&self) -> f64 {
        self.freq
    }

    pub fn read(&mut self, buffer: &mut [f64]) {
        let b = &self.bucket;

        let fmfreq = if R::FMScale.get_bool(b) {
            (R::Pitchmod.get(b) / 4.0).exp() - 1.0
        } else {
            R::Pitchmod.get(b)
        };

        if self.is_unison_controlled {
            let freq = R::Frequency.get(b);
            self.ff = freq;
            self.freq = freq;
        }
        
        let lpf_mod_rate = R::LPFmodrate.get(b);
        let amp_lfo_rate = R::AmpLFOrate.get(b);
        self.osc_fundamental_nco.switch_wave(b.get_enum(R::WFunction));
        self.osc_fundamental_nco.set_mod_len(self.sr / self.ff);
        self.osc_harmonics_nco.switch_wave(b.get_enum(R::HarmonicFunction));
        self.osc_harmonics_nco.set_mod_len(self.sr / lpf_mod_rate);

        for sample in buffer {
            let mut fund = if R::PitchmodWidth.get(b) * fmfreq != 0.0 {
                self.ff * (1.0 + R::PitchmodWidth.get(b) * self.osc_freq_mod.peek(fmfreq, 1.0))
            } else { self.ff };

            fund *= R::PitchBendFactor.get(b);

            self.osc_fundamental_nco.incr(self.ff);
            self.osc_harmonics_nco.incr(self.ff);
            self.osc_subosc_nco.incr(self.ff * 0.5);
            self.osc_filter_mod.incr(lpf_mod_rate);
            self.osc_freq_mod.incr(fmfreq);
            self.osc_ampl_mod.incr(amp_lfo_rate);

            let mut ampl = self.adsr_ampl.next_sample();
            let max_vel = R::MaxVelocity.get(b);
            ampl *= R::GeneralAtten.get(b)
                * 0.5
                * (f64::from(min(self.vel as u32, max_vel as u32)) / max_vel)
                / self.ampl_fact;

            if (ampl * f64::from(u16::max_value())) as i32 == 0 {
                self.adsr_filter_duck.next_sample();
                self.adsr_filter_mod.next_sample();
                continue;
            }

            let lfo_width = R::AmpLFOwidth.get(b);
            if lfo_width > 0.0 && ampl > 0.0 {
                ampl *= 1.0 - lfo_width * 0.5 * (1.0 - self.osc_ampl_mod.peek(amp_lfo_rate, 1.0));
            }

            let mut s = 0.0;
            if ampl > 0.0 {
                s = self.osc_fundamental_nco.peek(fund, ampl);
                let x = if R::HarmonicV1.get_bool(b) { 1 } else { 0 };

                let max_harmonic = R::MaxHarmonic.get(b);
                let start = 2 + x;
                let end = max_harmonic as u32 + x * 2;
                for ii in start..end {
                    ampl *= CONST_AFF_VALUE * R::HarmonicsControl.get(b);
                    let mut vol = ampl;
                    if ii == 2 {
                        if R::HarmonicFix.get_bool(b) {
                            break
                        } else {
                            vol = (ampl * 10.0 - vol) * R::Harmonic2Gain.get(b) + vol;
                        }
                    }
                    let phase = f64::from(ii - x) * (self.sr / fund) * R::HarmonicPhase.get(b) / max_harmonic;
                    s += self.osc_harmonics_nco.peek_offseted(fund * f64::from(ii / (x + 1)), vol, phase);
                }

                let sub_osc_gain = R::SubOscGain.get(b);
                if sub_osc_gain > 0.0 {
                    s += self.osc_subosc_nco.peek(fund * 0.5, ampl * sub_osc_gain);
                }
            }

            let lpf_mod_rate = R::LPFmodrate.get(b);
            let sv = if lpf_mod_rate > 0.0 { self.osc_filter_mod.peek(lpf_mod_rate, 1.0) } else { 1.0 };

            self.lpf = if R::LPFenvelope.get_bool(b) {
                R::LPFfloor.get(b) + (R::LPFceiling.get(b) - R::LPFfloor.get(b)) * self.adsr_filter_mod.next_sample()
            } else { R::LPF.get(b) };

            self.lpf += R::LPFwidth.get(b) * sv;
            self.lpf += (CONST_LPF_DUCK_FREQ - self.lpf) * self.adsr_filter_duck.next_sample();
            if self.lpf < 1.0 { self.lpf = 1.0; }

            self.filter_lpf.set_cutoff(self.lpf);
            s = self.filter_lpf.process(s);

            let hpf_cutoff = R::HPFCutoff.get(b);
            if hpf_cutoff > 0.0 {
                s = self.filter_hpf.process(s, hpf_cutoff);
            }
            *sample += s * R::Gain.get(b);
        }
    }
    
    pub fn refresh_max_harmonic(&mut self) {
        let b = &mut self.bucket;

        let mut total = self.ampl_fact;
        let max_harmonic = R::MaxHarmonic.get(b) as u32;

        for _ in 1..max_harmonic {
            self.ampl_fact *= CONST_AFF_VALUE;
            total += self.ampl_fact;
        }

        self.ampl_fact = total;
    }

    pub fn note_on(&mut self) {
        self.adsr_ampl.note_on();
        self.adsr_filter_mod.note_on();
    }

    pub fn note_off(&mut self) {
        self.adsr_ampl.note_off();
        self.adsr_filter_mod.note_off();
    }

    pub fn refresh(&mut self) {
        let b = &self.bucket;
        let adsr_ampl_a = R::Attack.get(b);
        let adsr_ampl_d = R::Decay.get(b);
        let adsr_ampl_s = R::Sustain.get(b);
        let adsr_ampl_r = R::Release.get(b);

        self.adsr_ampl.modify(
            adsr_ampl_a,
            adsr_ampl_d,
            adsr_ampl_s,
            adsr_ampl_r
        );

        self.adsr_filter_mod.modify(R::LPFattack.get(b), 0.0, 1.0, R::LPFrelease.get(b));

        self.filter_lpf.set_cutoff(self.lpf);
    }

    pub fn is_running(&self) -> bool {
        self.adsr_ampl.active()
    }
}

impl Hash for SynthVoice {
    fn hash<H: Hasher>(&self, state: &mut H) {
        state.write_usize(self.iden);
        state.write_usize(self.bucket.as_raw() as usize);
    }
}

impl PartialEq<SynthVoice> for SynthVoice {
    fn eq(&self, other: &SynthVoice) -> bool {
        self.iden == other.iden && self.bucket.as_raw() == other.bucket.as_raw()
    }
}

impl Eq for SynthVoice {}
