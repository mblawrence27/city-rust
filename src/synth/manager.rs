use std::iter::Iterator;
use std::cell::RefCell;
use std::rc::Rc;

use crate::synth::sender::{KeyData, MidiData, Message};
use crate::synth::voice::SynthVoice;
use crate::utils::parameters::R;
use crate::utils::queue::MessageQueue;
use crate::utils::state::StateManager;

use super::{keys, VERBOSE_PRINT};

const BUFFER_LENGTH: usize = 64;
const FREQUENCY_A4_NOTE: f64 = 440.0;

type VoiceFactory = Box<dyn Fn(usize) -> SynthVoice>;
type MsgQueue = MessageQueue<Message>;

struct VoiceItem {
    voice: SynthVoice,
    active: bool,
    iden: Option<u8>
}

pub struct VoiceScheduler {
    voice_pool: Vec<VoiceItem>,
    queue: Rc<RefCell<MsgQueue>>,
    bucket: StateManager<R, f64>,
    buf: Vec<Option<Message>>,
}

impl VoiceScheduler {
    pub fn new(queue: Rc<RefCell<MsgQueue>>, voice_factory: VoiceFactory, max_count: usize) -> Self {
        let mut voice_pool = Vec::with_capacity(max_count);
        for i in 0..max_count {
            let item = VoiceItem {
                voice: voice_factory(i),
                active: false,
                iden: None
            };
            voice_pool.push(item);
        }
        
        let bucket = crate::utils::state::StateManager::new();
        let buf = std::iter::repeat_with(|| None).take(BUFFER_LENGTH).collect();
        Self {
            voice_pool,
            queue,
            bucket,
            buf,
        }
    }

    pub fn voices<'a>(&'a self) -> Box<dyn Iterator<Item = &SynthVoice> + 'a> {
        Box::new(self.voice_pool.iter().map(|item| &item.voice))
    }

    pub fn voices_mut<'a>(&'a mut self) -> Box<dyn Iterator<Item = &mut SynthVoice> + 'a> {
        Box::new(self.voice_pool.iter_mut().map(|item| &mut item.voice))
    }

    pub fn has_active_voices(&self) -> bool {
        self.voice_pool.iter()
            .find(|item| item.active || item.voice.is_running())
            .is_some()
    }

    pub fn process_updates(&mut self) {
        let count = if let Ok(mut queue) = self.queue.try_borrow_mut() {
            let mut idx = 0;
            for msg in queue.collect() {
                if let Some(msg) = msg.take() {
                    self.buf[idx] = Some(msg);
                    idx += 1;
                }
            }
            idx
        } else {
            0
        };

        for idx in 0..count {
            let msg = self.buf[idx].take().unwrap_or(Message::NoOp);
            match msg {
                Message::NoteOn(data) => {
                    let voice_idx = self.get_unused_voice();
                    let freq = VoiceScheduler::get_key_code_freq(&data);
                    VoiceScheduler::activate_voice(voice_idx, Some(data.key_code), freq, &mut self.voice_pool);
                },
                Message::NoteOff(data) => {
                    let voice_idx = self.get_active_voice(data.key_code);
                    VoiceScheduler::deactivate_voice(voice_idx, Some(data.key_code), &mut self.voice_pool);
                },
                Message::MidiNoteOn(data) => {
                    let voice_idx = self.get_unused_voice();
                    let freq = VoiceScheduler::get_midi_note_freq(&data);
                    VoiceScheduler::activate_voice(voice_idx, Some(data.midi_note), freq, &mut self.voice_pool)
                },
                Message::MidiNoteOff(data) => {
                    let voice_idx = self.get_active_voice(data.midi_note);
                    VoiceScheduler::deactivate_voice(voice_idx, Some(data.midi_note), &mut self.voice_pool);
                },
                Message::PitchBend(iden, amount) => {
                    let voice_idx = self.get_active_voice(iden);
                    VoiceScheduler::control_voice_bend(voice_idx, amount, &mut self.voice_pool);
                }
                Message::StateUpdate((k, v)) => {
                    self.bucket.set(k, v);
                }
                Message::StateFill(cloned) => {
                    for kvp in cloned.inner.entries() {
                        self.bucket.set(kvp.0, kvp.1);
                    }
                }
                Message::NoOp => (),
            }
        }
    }

    fn get_key_code_freq(data: &KeyData) -> f64 {
        let semitone_offset = keys::st_from_key_code(data.key_code);
        let a4_freq = FREQUENCY_A4_NOTE * 2.0_f64.powi(data.octave.unwrap_or(0));
        keys::semitones_to_factor(semitone_offset) * a4_freq
    }

    fn get_midi_note_freq(data: &MidiData) -> f64 {
        const A4_PITCH: i8 = 69;
        ((f64::from(data.midi_note as i8 - A4_PITCH)) / 12.).exp2() * FREQUENCY_A4_NOTE
    }

    fn activate_voice(voice_idx: Option<usize>, iden: Option<u8>, freq: f64, voice_pool: &mut Vec<VoiceItem>) {
        if VERBOSE_PRINT {
            println!("voice activated -> note on received: {:?} | voice idx: {:?}", (iden, freq), voice_idx);
        }
        if let Some(voice_idx) = voice_idx {
            let voice_item = &mut voice_pool[voice_idx];
            voice_item.iden = iden;
            voice_item.active = true;
            voice_item.voice.set_freq(freq);
            voice_item.voice.note_on();
        } else {
            panic!("no free voice available to process note on event: {:#?}", (iden, freq));
            // TODO: use last or first edited note, use lowest velocity, use closest freq note
        }
    }

    fn deactivate_voice(voice_idx: Option<usize>, iden: Option<u8>, voice_pool: &mut Vec<VoiceItem>) {
        if VERBOSE_PRINT {
            println!("voice deactivated -> note off received: {:?} | voice idx: {:?}", iden, voice_idx);
        }
        if let Some(voice_idx) = voice_idx {
            let voice_item = &mut voice_pool[voice_idx];
            voice_item.iden = None;
            voice_item.active = false;
            voice_item.voice.note_off();
        } else if VERBOSE_PRINT {
            println!("no active voice found to process note off event: {:#?}", iden);
        }
    }

    fn control_voice_bend(voice_idx: Option<usize>, amount: f64, voice_pool: &mut Vec<VoiceItem>) {
        if let Some(voice_idx) = voice_idx {
            let voice_item = &mut voice_pool[voice_idx];
            let semitones = Some(amount);
            voice_item.voice.set_freq_now(voice_item.voice.freq() * keys::semitones_to_factor(semitones));
        }
    }

    fn get_unused_voice(&self) -> Option<usize> {
        let mut voice_idx = None;
        for (idx, item) in self.voice_pool.iter().enumerate() {
            if !item.active {
                if item.voice.is_running() {
                    voice_idx = Some(idx);
                    if VERBOSE_PRINT {
                        println!("tentatively leaving {} as voice is still running", idx);
                    }
                } else {
                    return Some(idx);
                }
            } else if VERBOSE_PRINT {
                println!("skipping {} as voice is active", idx);
            }
        }
        voice_idx
    }

    fn get_active_voice(&self, iden: u8) -> Option<usize> {
        self.voice_pool.iter()
            .position(|item| item.iden == Some(iden))
    }
}
