pub extern crate cityrust;

pub mod helpers;

#[cfg(feature = "jack")] mod jack;
#[cfg(feature = "jack")] use crate::jack::use_jack as run;

#[cfg(not(feature = "jack"))] mod socket;
#[cfg(not(feature = "jack"))] use crate::socket::use_socket as run;

fn main() {
    run();
}
