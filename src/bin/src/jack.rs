extern crate jack;

use super::cityrust::dsp::butterworth::ButterworthFilter;
use jack::{AudioIn, AudioOut, Client, ClientOptions, Control, Port, ProcessHandler, ProcessScope};

use std::io;

pub fn use_jack() {
    println!("starting in jack mode...");
    let c_res = Client::new("rusty_client", ClientOptions::NO_START_SERVER);
    match c_res {
        Ok((client, status)) => {
            println!("Managed to open client {}, with status {:?}!", client.name(), status);
            let plugin = Plugin::new(&client);
            let _ac = client.activate_async((), plugin)
                .unwrap();

            let mut msg = ControlMessage::NoOp;
            while msg != ControlMessage::Exit {
                msg = handle_control_keys();
            }
        },
        Err(e) => println!("Failed to open client because of error: {:?}", e),
    };
}

struct Plugin {
    audio_in_port: Port<AudioIn>,
    audio_out_port: Port<AudioOut>,
    
    filter: Option<ButterworthFilter>,
}

impl Plugin {
    fn new(client: &Client) -> Self {
        let audio_in_port = client.register_port("in", jack::AudioIn::default()).unwrap();
        println!("audio_in_port:  {:#?}", audio_in_port);
        let audio_out_port = client.register_port("out", jack::AudioOut::default()).unwrap();
        println!("audio_out_port: {:#?}", audio_out_port);

        let sr = client.sample_rate();
        let sr = f64::from(sr);

        let filter = Plugin::init_filter(4, sr, 1000.0);

        Plugin { audio_in_port, audio_out_port, filter }
    }

    
    fn init_filter(poles: usize, sr: f64, fc: f64) -> Option<ButterworthFilter> {
        let mut filter = ButterworthFilter::new(poles, sr);
        filter.set_cutoff(fc);
        Some(filter)
    }
}

impl ProcessHandler for Plugin {
    fn process(&mut self, _: &Client, process_scope: &ProcessScope) -> Control {
        let in_buffer = self.audio_in_port.as_slice(process_scope);
        let out_buffer = self.audio_out_port.as_mut_slice(process_scope);

        let buffer = in_buffer.iter().zip(out_buffer);
        let filter: &mut ButterworthFilter = self.filter.as_mut().unwrap();

        for (input_sample, output_sample) in buffer {
            *output_sample = filter.process(*input_sample as f64) as f32;
        }

        Control::Continue
    }
}


#[derive(PartialEq)]
enum ControlMessage {
    NoOp,
    Exit
}

fn handle_control_keys() -> ControlMessage {
    let mut user_input = String::new();
    let cleaned = match io::stdin().read_line(&mut user_input) {
        Ok(_) => Some(user_input.trim().to_lowercase()),
        Err(_) => None,
    };

    match &cleaned {
        Some(x) if x == &String::from("q") => ControlMessage::Exit,
        _ => ControlMessage::NoOp
    }
}