use crate::socket::SHOULD_PRINT_VERBOSE;
use crate::helpers::clone_into_array;

use crate::cityrust::dsp::reverb::Reverb;

use std::io::prelude::*;
use std::mem;
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;

use std::{f64, u64};

pub fn start_server() {
    let (ip_address, port) = ("0.0.0.0", "27017");
    let full_address = format!("{}:{}", ip_address, port);
    let listener = TcpListener::bind(&full_address).unwrap();

    println!("we've now bound to {}", full_address);
    loop {
        for stream in listener.incoming() {
            let stream = stream.unwrap();
            let _ = thread::spawn(|| {
                handle_connection(stream);
            });
        }
    }
}

fn handle_connection(mut stream: TcpStream) {
    println!("we've got a connection");
    let mut reverb = Reverb::new(44100, vec![3501, 130, 43, 14, 4]);
    let size = mem::size_of::<f64>();
    let mut buffer = [0; 512];
    let mut out_buffer = [0; 512];
    let mut max_sample = 0.0;
    let mut times_sent = 0;
    loop {
        let count = stream.read(&mut buffer).unwrap();
        let samples = buffer.chunks_exact(size).take(count / size).map(|bytes| {
            let b = clone_into_array(bytes);
            let bits = u64::from_le_bytes(b);
            let f = f64::from_bits(bits);
            if f > max_sample {
                max_sample = f;
            }
            if SHOULD_PRINT_VERBOSE {
                println!("received {:?}: {:.5e}", b, f);
            }
            f
        });

        let done_samples = samples
            .map(|sample| reverb.get_sample(sample, 0.99))
            .enumerate()
            .collect::<Vec<_>>();
        if SHOULD_PRINT_VERBOSE {
            println!(
                "\nwe've a ton of samples, {} infact from an initial {} bytes\n",
                done_samples.len(),
                count
            );
        }

        for (i, sample) in done_samples {
            let sample = sample / max_sample;
            if SHOULD_PRINT_VERBOSE {
                println!("processing {}: {:.5e}", i, sample);
            }
            let idx = i * size;
            let bits = sample.to_bits();
            let bytes = bits.to_le_bytes();
            out_buffer[idx..(size + idx)].clone_from_slice(&bytes[..size]);
        }

        times_sent += 1;
        if times_sent > 1 && SHOULD_PRINT_VERBOSE {
            println!(
                "btw you have now sent {} times on this connection",
                times_sent
            );
        }

        stream.write_all(&out_buffer).unwrap();
        stream.flush().unwrap();
    }
}
