extern crate byteorder;
mod client;
mod server;

use std::io::prelude::*;

pub const SHOULD_PRINT_VERBOSE: bool = true;
// pub const InitalState: Option<RunState> = None;
const INITAL_STATE: Option<RunState> = Some(RunState::Client);

#[derive(PartialEq)]
enum RunState {
    Server,
    Client,
}

pub fn use_socket() {
    println!("Starting in socket mode.\n");

    let mut char_buf = [0; 1];
    let mut run_state = INITAL_STATE;

    if run_state.is_some() {
        process_state(&run_state);
    } else {
        println!("To begin network tests, please enter 's' or 'c'...");

        while run_state.is_none() {
            std::io::stdin().read_exact(&mut char_buf).unwrap();

            match char_buf[0] as char {
                m if m == 's' => {
                    println!("Starting in server mode...");
                    run_state = Some(RunState::Server);
                },
                m if m == 'c' => {
                    println!("Starting in client mode...");
                    run_state = Some(RunState::Client);
                },
                _ => println!("Please enter 's' for server mode or 'c' for client mode...")
            }

            process_state(&run_state);
        }
    }

    println!("Exiting.");
}

fn process_state(run_state: &Option<RunState>) {
    match run_state {
        Some(RunState::Server) => server::start_server(),
        Some(RunState::Client) => client::start_client(),
        _ => (),
    }
}