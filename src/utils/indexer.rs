pub struct WrappingIndex {
    current: f64,
    previous: f64,
    wrap_len: Option<f64>
}

impl WrappingIndex {
    pub fn new(value: f64) -> WrappingIndex {
        assert!(value >= 0.0, "index value must be positive");
        WrappingIndex {
            current: value,
            previous: value,
            wrap_len: None
        }
    }

    #[inline]
    pub fn current(&self) -> f64 {
        self.current
    }

    #[inline]
    pub fn previous(&self) -> f64 {
        self.previous
    }

    pub fn set(&mut self, value: f64) -> usize {
        assert!(value >= 0.0, "index value must be positive");
        self.previous = self.current;
        self.current = value;
        self.current as usize
    }

    pub fn wrap_increment(&mut self, increment: f64, wrap_len: f64) {
        self.validate_params(wrap_len);
        self.wrap_values(increment, wrap_len);
    }

    pub fn mod_increment(&mut self, increment: f64, wrap_len: f64) {
        if self.current < wrap_len {
            self.wrap_values(increment, wrap_len);
        } else {
            self.wrap_values_mod(increment, wrap_len);
        }
    }

    pub fn set_wrap_len(&mut self, wrap_len: f64) {
        self.validate_params(wrap_len);
        self.wrap_len = Some(wrap_len);
    }

    #[inline]
    pub fn set_wrap_len_unchecked(&mut self, wrap_len: f64) {
        self.wrap_len = Some(wrap_len);
    }

    pub fn next_sample(&mut self) -> f64 {
        self.wrap_values(1.0, self.wrap_len.unwrap());
        self.current
    }

    pub fn peek_offsetted(&self, offset: f64) -> f64 {
        let wrap_len = self.wrap_len.unwrap();
        assert!(offset < wrap_len, "provided offset must be less than the wrapping length");
        let offsetted = self.current + offset; 
        if offsetted >= wrap_len {
            offsetted - wrap_len
        }
        else if offsetted + offset < 0.0 {
            offsetted + wrap_len
        } else { offsetted }
    }

    pub fn reset_wrap_len(&mut self, wrap_len: f64) {
        assert!(
            wrap_len >= 0.0,
            "the length over which the increment is wrapped must be positive"
        );
        self.previous %= wrap_len;
        self.current %= wrap_len;
    }

    pub fn wrap_values(&mut self, increment: f64, wrap_len: f64) {
        self.previous = self.current;
        self.current += increment;
        if self.current >= wrap_len {
            self.current -= wrap_len;
        }
        if self.current < 0.0 {
            self.current += wrap_len;
        }
    }

    fn wrap_values_mod(&mut self, increment: f64, wrap_len: f64) {
        self.previous = self.current;
        self.current = (self.current + increment) % wrap_len;
    }

    fn validate_params(&self, wrap_len: f64) {
        assert!(
            wrap_len >= 0.0,
            "the length over which the increment is wrapped must be positive"
        );
        assert!(
            self.current < wrap_len,
            "both current and previous value must be set to less than the wrapping length"
        );
        assert!(
            self.previous < wrap_len,
            "previous value must be less than the wrapping length"
        );
        assert!(
            self.current() >= 0.0 && self.previous >= 0.0,
            "both current and previous value must be less than the wrapping length"
        );
    }

    #[inline]
    pub fn as_usize(&self) -> (usize, usize) {
        (self.current as usize, self.previous as usize)
    }

    #[inline]
    pub fn idx(&self) -> usize {
        self.current as usize
    }
}