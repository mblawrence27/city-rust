
use std::sync::mpsc::{self, Receiver, Sender};

const DEFAULT_BUFFER_LENGTH: usize = 64;

pub struct MessageQueue<T> {
    rx: Receiver<T>,
    tx: Sender<T>,
    buf: Vec<Option<T>>,
}

impl<T> Default for MessageQueue<T> {
    fn default() -> Self {
        Self::new(DEFAULT_BUFFER_LENGTH)
    }
}

impl<T> MessageQueue<T> {
    pub fn new(buffer_length: usize) -> Self {
        let (tx, rx) = mpsc::channel();
        Self {
            rx, 
            tx,
            buf: std::iter::repeat_with(|| None).take(buffer_length).collect(),
        }
    }

    pub fn dispatch(&self) -> Dispatcher<T> {
        Dispatcher(self.tx.clone())
    }

    pub fn collect(&mut self) -> &mut [Option<T>] {
        let mut count = 0;

        let buffer_length = self.buf.len();
        for (idx, msg) in self.rx.try_iter().enumerate() {
            self.buf[idx] = Some(msg);
            count += 1;
            if count == buffer_length {
                break;
            }
        }
        &mut self.buf[..count]
    }
}

pub struct Dispatcher<T>(Sender<T>);

impl<T> Dispatcher<T> {
    pub fn send(&self, msg: T) -> Result<(), ()> {
        self.0.send(msg).map_err(|_| ())
    }
}
