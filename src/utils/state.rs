use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;
use std::iter::Iterator;
use std::ops::Deref;
use std::rc::{Rc};

use enum_map::{Enum, EnumMap};

use mappings::PresetMapping;

#[derive(Clone)]
pub struct StateManager<K, T> where T: Default + Copy, K: StateKeys<T> {
    bucket: Rc<RefCell<Bucket<K, T>>>
}

impl<K, T> StateManager<K, T> where K: StateKeys<T>, T: Into<f64> + Default + Copy {
    pub fn get_integer(&self, key: K) -> u32 {
        self.get(key).into() as u32
    }
    pub fn get_enum<E: Enum<()>>(&self, key: K) -> E {
        Enum::<()>::from_usize(self.get_integer(key) as usize)
    }
}

impl<K, T: Default + Copy> StateManager<K, T> where K: StateKeys<T> {
    pub fn new() -> Self {
        let bucket_store = Bucket::new();
        let bucket = Rc::new(RefCell::new(bucket_store));

        StateManager {
            bucket,
        }
    }

    pub fn get(&self, key: K) -> T {
        self.bucket.borrow().get(key)
    }

    pub fn get_all(&self) -> HashMap<K, T> {
        self.bucket.borrow().get_all()
    }

    pub fn set(&mut self, key: K, value: T) {
        self.bucket.borrow_mut().set(key, value);
    }

    pub fn size(&self) -> usize {
        self.bucket.borrow().size()
    }

    pub fn clear_all(&mut self) {
        self.bucket.borrow_mut().clear();
    }

    pub fn as_raw(&self) -> *const Bucket<K, T> {
        self.bucket.borrow().deref() as *const Bucket<K, T>
    }

    pub fn borrowed_clone(&self) -> Self {
        let bucket = self.bucket.clone();

        StateManager {
            bucket,
        }
    }

    pub fn inner_clone(&self) -> Bucket<K, T> {
        self.bucket.borrow().clone()
    }
}

impl<K, T: Default + Copy> StateManager<K, T> where K: InitailState<T> {
    pub fn load(&mut self, values: &EnumMap<K, T>) {
        for (key, value) in values {
            self.bucket.borrow_mut().set(key, *value);
        }
    }
}

impl<K, T> Default for StateManager<K, T> where K: StateKeys<T>, T: Into<u32> + Default + Copy {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Default, Debug, Clone)]
pub struct Bucket<K, T: Default + Copy> where K: StateKeys<T> {
    store: Vec<Option<T>>,
    _phantom: std::marker::PhantomData<K>
}

impl<K, T: Default + Copy> Bucket<K, T> where K: StateKeys<T> {
    pub fn new() -> Self {
        let count = K::POSSIBLE_VALUES;
        let store: Vec<Option<T>> = std::iter::repeat(None).take(count).collect();
        Bucket {
            store,
            _phantom: std::marker::PhantomData,
        }
    }

    pub fn get(&self, key: K) -> T {
        self.store[K::to_usize(key)].unwrap_or_default()
    }

    pub fn set(&mut self, key: K, value: T) {
        self.store[K::to_usize(key)] = Some(value);
    }

    pub fn get_all(&self) -> HashMap<K, T> {
        self.store.iter()
            .enumerate()
            .filter(|(_, value)| value.is_some())
            .map(|(idx, value)| {
                (K::from_usize(idx), value.unwrap())
            })
            .collect()
    }

    pub fn entries(&self) -> Box<dyn Iterator<Item = (K, T)> + '_> {
        Box::new(
            self.store.iter()
                .enumerate()
                .filter(|(_, value)| value.is_some())
                .map(|(idx, value)| {
                    (K::from_usize(idx), value.unwrap())
                })
        )
    }

    pub fn size(&self) -> usize {
        self.store.iter().filter(|value| value.is_some()).count()
    }

    pub fn clear(&mut self) {
        for value in self.store.iter_mut() {
            *value = None;
        }
    }
}

pub trait StateKeys<T: Default + Copy> : Copy + Debug + Enum<T> + Eq + Hash {
    fn get(&self, state: &StateManager<Self, T>) -> T {
        state.get(*self)
    }

    fn get_bool(&self, state: &StateManager<Self, T>) -> bool where T: Into<f64> {
        state.get(*self).into() >= 1.0_f64
    }

    fn set(&self, state: &mut StateManager<Self, T>, value: T) {
        state.set(*self, value);
    }
}

pub trait InitailState<T: Default + Copy> : Enum<T> + StateKeys<T> {
    fn initial() -> EnumMap<Self, T>;
}

pub trait BoundedState<T: Default + Copy> : Enum<Option<(T, T)>> + StateKeys<T> {
    fn bounds() -> EnumMap<Self, Option<(T, T)>>;
}

pub trait PresetState<T: Default + Copy> : Enum<PresetMapping<T>> + StateKeys<T> {
    fn mapping() -> EnumMap<Self, PresetMapping<T>>;
}

pub mod mappings {
    use std::fmt::{self, Debug};

    pub type ValueTransformFn<T> = Box<dyn Fn(T) -> T>;

    pub struct PresetMapping<T> {
        pub command_iden: Option<&'static str>,
        pub value_transform: Option<ValueTransformFn<T>>,
        pub export_transform: Option<ValueTransformFn<T>>,
    }

    impl<T> Debug for PresetMapping<T> {
        fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(fmt, "({:?}, {:?})",
                self.command_iden,
                if let Some(_) = self.value_transform { "SomeTransform" } else { "None" }
            )
        }
    }

    pub fn map<T>(command_iden: Option<&'static str>, value_transform: Option<ValueTransformFn<T>>) -> PresetMapping<T> {
        PresetMapping {
            command_iden,
            value_transform,
            export_transform: None
        }
    }
    pub fn both<T>(command_iden: Option<&'static str>,
        value_transform: Option<ValueTransformFn<T>>,
        export_transform: Option<ValueTransformFn<T>>) -> PresetMapping<T>
    {
        PresetMapping {
            command_iden,
            value_transform,
            export_transform
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Copy, Clone, Debug, Enum, Eq, PartialEq, Hash)]
    pub enum R {
        Attack,
        Decay,
        Release,
        Sustain,
    }

    impl StateKeys<f64> for R {}

    #[test]
    fn state_can_create_manager() {
        let manager: StateManager<R, f64> = StateManager::new();

        assert_eq!(manager.size(), 0);
    }

    #[test]
    fn state_can_add_items() {
        let mut manager = StateManager::new();
        let (a, d, s, r) = (1.0, 0.2, 0.6, 0.8);

        manager.set(R::Attack, a);
        manager.set(R::Decay, d);
        manager.set(R::Sustain, s);
        manager.set(R::Release, r);

        assert_eq!(manager.size(), 4);

        assert_eq!(manager.get(R::Attack), a);
        assert_eq!(manager.get(R::Decay), d);
        assert_eq!(manager.get(R::Sustain), s);
        assert_eq!(manager.get(R::Release), r);

        manager.clear_all();

        assert_eq!(manager.size(), 0);
    }

    #[test]
    fn state_can_add_items_cloned() {
        let mut manager = StateManager::new();
        let (a, d, s, r) = (1.0, 0.2, 0.6, 0.8);

        manager.set(R::Attack, a);

        let gen_func = |r, value| {
            let mut cloned = manager.borrowed_clone();
            move || {
                cloned.set(r, value);
            }
        };
        
        gen_func(R::Sustain, s)();
        gen_func(R::Release, r)();
        gen_func(R::Decay, d)();

        assert_eq!(manager.get(R::Attack), a);
        assert_eq!(manager.get(R::Decay), d);
        assert_eq!(manager.get(R::Sustain), s);
        assert_eq!(manager.get(R::Release), r);
        assert_eq!(manager.size(), 4);
    }
}
