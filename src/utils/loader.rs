use crate::utils::state::{InitailState, PresetState, StateKeys, StateManager};
use crate::utils::state::mappings::PresetMapping;
use crate::utils::preset::{Preset, PresetCommand};

pub fn load_defaults<K, T>(state: &mut StateManager<K, T>)
    where T: Default + Copy, K: InitailState<T> + StateKeys<T>
{
    let initial = K::initial();
    for (key, value) in initial {
        key.set(state, value);
    }
}

pub fn load_single_command<'a, K>(state: &mut StateManager<K, f64>, command: &'a PresetCommand)
    -> Result<(), &'a PresetCommand> where K: PresetState<f64> + StateKeys<f64>
{
    if let Some(value) = command.value {
        for (key, mapping) in &K::mapping() {
            if match_command(command, mapping) {
                let value = apply_transform(mapping, f64::from(value));
                key.set(state, value);
                return Ok(());
            }
        }
    }
    Err(command)
}

pub fn load_commands<'a, K>(state: &mut StateManager<K, f64>, commands: &'a[PresetCommand])
    -> Result<(), Vec<&'a PresetCommand>> where K: PresetState<f64> + StateKeys<f64>
{
    let mappings = K::mapping();
    let mut err_commands = vec![];

    for command in commands {
        if let Some(value) = command.value {
            let matched = mappings.iter()
                .find(|(_, mapping)| match_command(command, &mapping));
            if let Some((key, mapping)) = &matched {
                let value = apply_transform(mapping, f64::from(value));
                key.set(state, value);
            } else {
                err_commands.push(command);
            }
        } else {
            err_commands.push(command);
        }
    }

    if err_commands.is_empty() {
        Ok(())
    } else {
        Err(err_commands)
    }
}

pub fn save_preset<'a, K>(state: &StateManager<K, f64>, preset_name: String) -> Result<Preset, ()>
    where K: PresetState<f64> + StateKeys<f64>
{
    let mappings = K::mapping();
    let state_dump = state.get_all();

    let mappings = mappings.into_iter()
        .filter(|(_, mapping)| mapping.command_iden.is_some());

    let mut commands = vec![];
    
    for kvp in mappings {
        let matched = state_dump.get(&kvp.0);
        if let Some(matched) = matched {
            let preset_mapping = kvp.1;
            let opcode = preset_mapping.command_iden.unwrap()
                .to_string();
            let preset_value = if let Some(transform) = preset_mapping.export_transform {
                transform(*matched) as f32
            } else {
                *matched as f32
            };

            commands.push(PresetCommand::exact(opcode, preset_value));
        }
    }

    Ok(Preset::new(preset_name, commands))
}

fn match_command<T>(command: &PresetCommand, mapping: &PresetMapping<T>) -> bool {
    if let Some(command_iden) = mapping.command_iden {
        command_iden == command.opcode
    } else {
        false
    }
}

fn apply_transform<T: Default + Copy>(mapping: &PresetMapping<T>, value: T) -> T {
    if let Some(value_transform) = &mapping.value_transform {
        value_transform(value)
    } else {
        value
    }
}

#[cfg(test)]
mod test {
    use enum_map::{Enum, EnumMap};

    use crate::dsp::oscillator::Waveform;
    use crate::utils::state::mappings::{both, map};
    use crate::utils::parameters::tranformations::{transform_from_db, transform_to_db};
    use crate::utils::preset::Preset;

    use super::*;

    const ADSR_DEFAULTS: (f64, f64, f64, f64) = (30.0, 30.0, 0.0, 50.0);

    #[derive(Copy, Clone, Debug, Eq, PartialEq, Hash, Enum)]
    pub enum R {
        Attack,
        Decay,
        Sustain,
        Release,
    }

    impl StateKeys<f64> for R {}

    impl PresetState<f64> for R {
        fn mapping() -> EnumMap<Self, PresetMapping<f64>> {
            enum_map! {
                R::Attack  => map(Some("a"), None),
                R::Decay   => map(Some("d"), None),
                R::Sustain => map(Some("s"), None),
                R::Release => map(Some("r"), None),
            }
        }
    }
    
    impl InitailState<f64> for R {
        fn initial() -> EnumMap<Self, f64> {
            enum_map! {
                R::Attack  => ADSR_DEFAULTS.0,
                R::Decay   => ADSR_DEFAULTS.1,
                R::Sustain => ADSR_DEFAULTS.2,
                R::Release => ADSR_DEFAULTS.3,
            }
        }
    }

    #[test]
    fn loader_can_load_defaults() {
        let mut manager = StateManager::new();
        load_defaults(&mut manager);

        assert_eq!(manager.get(R::Attack),  ADSR_DEFAULTS.0);
        assert_eq!(manager.get(R::Decay),   ADSR_DEFAULTS.1);
        assert_eq!(manager.get(R::Sustain), ADSR_DEFAULTS.2);
        assert_eq!(manager.get(R::Release), ADSR_DEFAULTS.3);
    }

    #[test]
    fn loader_can_load_single_preset() {
        let mut manager = StateManager::new();
        let (a, d, s, r) = (1.0, 40.0, 0.5, 2.0);
        let sample_data = String::from("a:1;d:40;s:0.5;r:2;fake:3");

        let preset = Preset::parse(String::new(), &sample_data);

        for command in preset.commands {
            let result = load_single_command(&mut manager, &command);
            if let Err(incorrect) = result {
                if incorrect.opcode != "fake" {
                    result.unwrap();
                }
            }
        }

        assert_eq!(manager.get(R::Attack),  a);
        assert_eq!(manager.get(R::Decay),   d);
        assert_eq!(manager.get(R::Sustain), s);
        assert_eq!(manager.get(R::Release), r);
    }
    
    #[test]
    fn loader_can_load_bulk_presets() {
        let mut manager = StateManager::new();
        let (a, d, s, r) = (1.0, 40.0, 0.5, 2.0);
        let sample_data = String::from("a:1;d:40;s:0.5;r:2;fake:3");

        let preset = Preset::parse(String::new(), &sample_data);
        
        let result = load_commands(&mut manager, &preset.commands);

        if let Err(incorrects) = result {
            for incorrect in incorrects {
                if incorrect.opcode != "fake" {
                    panic!("found a rejected valid command: {:?}", incorrect);
                }
            }
        }

        assert_eq!(manager.get(R::Attack),  a);
        assert_eq!(manager.get(R::Decay),   d);
        assert_eq!(manager.get(R::Sustain), s);
        assert_eq!(manager.get(R::Release), r);
    }
    
    #[test]
    fn loader_can_save_presets() {
        let mut manager: StateManager<R, f64> = StateManager::new();
        let sample_name = String::from("Sample Data");
        let sample_data = String::from("a:1;d:40;s:0.5;r:2");

        let preset = Preset::parse(String::new(), &sample_data);
        
        load_commands(&mut manager, &preset.commands).unwrap();
        let saved_preset = save_preset(&manager, sample_name.clone()).unwrap();
        let saved_data = saved_preset.to_string();
        
        assert_eq!(saved_data, format!("{}|{}", sample_name, sample_data));
    }

    #[derive(Copy, Clone, Debug, Eq, PartialEq, Hash, Enum)]
    pub enum R1 {
        Wfunction,
        GeneralAtten,
    }

    impl StateKeys<f64> for R1 {}

    impl PresetState<f64> for R1 {
        fn mapping() -> EnumMap<Self, PresetMapping<f64>> {
            enum_map! {
                R1::Wfunction    =>  map(Some("w"), None),
                R1::GeneralAtten => both(Some("g"), Some(transform_from_db()), Some(transform_to_db())),
            }
        }
    }

    #[test]
    fn loader_can_load_enum_preset() {
        let mut manager = StateManager::new();
        let sample_data = String::from("w:2");

        let preset = Preset::parse(String::new(), &sample_data);
        let command = preset.commands.first().unwrap();
        load_single_command(&mut manager, command).unwrap();

        assert_eq!(manager.get_enum::<Waveform>(R1::Wfunction), Waveform::Tri);
    }

    #[test]
    fn loader_can_load_transformed_preset() {
        let mut manager = StateManager::new();
        let sample_data = String::from("g:-10");

        let preset = Preset::parse(String::new(), &sample_data);
        let command = preset.commands.first().unwrap();
        load_single_command(&mut manager, command).unwrap();

        assert_eq!(manager.get(R1::GeneralAtten), 0.1);
    }
    
    #[test]
    fn loader_can_save_transformed_preset() {
        let mut manager: StateManager<R1, f64> = StateManager::new();
        let sample_name = String::from("Sample Data");
        let sample_data = String::from("g:-10");

        let preset = Preset::parse(String::new(), &sample_data);
        
        load_commands(&mut manager, &preset.commands).unwrap();
        let saved_preset = save_preset(&manager, sample_name.clone()).unwrap();
        let saved_data = saved_preset.to_string();
        
        assert_eq!(saved_data, format!("{}|{}", sample_name, sample_data));
    }
}
