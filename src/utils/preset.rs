use std::error::Error;
use std::fmt::{self, Display};

#[derive(Debug)]
pub struct PresetCommand {
    pub opcode: String,
    pub value: Option<f32>,
}

impl PresetCommand {
    pub fn new(command: String, input: &str) -> PresetCommand {
        PresetCommand {
            opcode: command,
            value: match input.parse::<f32>() {
                Ok(x) => Some(x),
                Err(_) => None,
            },
        }
    }
    pub fn exact(command: String, input_value: f32) -> PresetCommand {
        PresetCommand {
            opcode: command,
            value: Some(input_value),
        }
    }
}

impl std::cmp::PartialEq for PresetCommand {
    fn eq(self: &PresetCommand, rhs: &PresetCommand) -> bool {
        self.value == rhs.value && self.opcode == rhs.opcode
    }
}

#[derive(Debug)]
pub struct Preset {
    pub name: String,
    pub commands: Vec<PresetCommand>,
}

impl Preset {
    pub fn parse(preset_name: String, preset_store_line: &str) -> Preset {
        Preset {
            name: preset_name,
            commands: preset_store_line
                .split(';')
                .filter(|pair| !pair.trim_end().is_empty())
                .filter_map(|pair| Preset::pair_to_command(pair))
                .collect(),
        }
    }

    pub fn new(preset_name: String, commands: Vec<PresetCommand>) -> Preset {
        Self {
            name: preset_name,
            commands
        }
    }

    pub fn process_line(line: &str) -> Result<Preset, PresetError> {
        let mut line_iter = line.trim().trim_end_matches(';').trim_end().split('|');

        let name_section = line_iter
            .next()
            .ok_or(PresetError("error extracting preset name section"))?;

        let data_section = line_iter
            .next()
            .ok_or(PresetError("error extracting preset data section"))?;

        let name = name_section
            .split(':')
            .nth(1)
            .or_else(|| Some(name_section))
            .ok_or(PresetError("error extracting preset name value"))?;

        let data = String::from(data_section);
        let preset = Preset::parse(String::from(name), &data);

        Ok(preset)
    }

    pub fn get_nth_line(preset_blob: &str, line_index: usize) -> Result<Preset, PresetError> {
        preset_blob
            .lines()
            .skip(line_index)
            .map(str::trim)
            .filter(|line| !line.starts_with("//") && !line.trim_end().is_empty())
            .next()
            .map(|line| Preset::process_line(line))
            .ok_or(PresetError("couldn't get line"))?
    }

    pub fn get_lines(preset_blob: &str) -> Vec<Preset> {
        preset_blob
            .lines()
            .map(str::trim)
            .filter(|line| !line.starts_with("//") && !line.trim_end().is_empty())
            .filter_map(|line| Preset::process_line(line).ok())
            .collect()
    }

    fn pair_to_command(pair: &str) -> Option<PresetCommand> {
        Preset::build_command(pair.split(':').collect::<Vec<&str>>())
    }

    fn build_command(parsed_pair: Vec<&str>) -> Option<PresetCommand> {
        if parsed_pair.len() != 2 {
            return None;
        }
        let cmd = String::from(parsed_pair[0]);
        let val = String::from(parsed_pair[1]);
        Some(PresetCommand::new(cmd, &val))
    }
}

impl Display for Preset {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let strs: Vec<_> = self.commands.iter()
            .filter(|cmd| cmd.value.is_some())
            .map(|cmd| format!("{}:{}", cmd.opcode, cmd.value.unwrap()))
            .collect();
        let str = strs.join(";");
        write!(f, "{}|{}", self.name, str)
    }
}

#[derive(Debug)]
pub struct PresetError (&'static str);

impl Error for PresetError {}

impl Display for PresetError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Error while processing preset: {}", self.0)
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use std::fs;
    fn get_sample_preset_line() -> &'static str {
        "33:Pulser:700|a:1;d:41.9;s:0.4943282;r:2;h:5;hc:0.7632653;w:2;b:-1;lpf:10502.5;lfo:0;prate:0;pwidth:0;lwidth:500;la:5;lr:5;lf:5;lc:5000;delay:170;dwet:0.187;filter:0;lpfenv:0"
    }
    fn get_sample_preset_blob() -> String {
        fs::read_to_string("src/assets/userpresets.sdp").expect("couldn't open preset blob")
    }

    #[test]
    fn preset_can_create() {
        let name = String::from("Preset Name");
        let sample_line = String::from("");

        let preset = Preset::parse(name.clone(), &sample_line);

        assert_eq!(preset.name, name);
        assert_eq!(preset.commands.len(), 0);
    }

    #[test]
    fn preset_can_parse_data() {
        let name = String::from("Preset Name");
        let sample_data = String::from("a:1;d:40;s:0.5;r:2");

        let preset = Preset::parse(name, &sample_data);

        assert_eq!(preset.commands.len(), 4);
        assert!(preset
            .commands
            .contains(&PresetCommand::new(String::from("a"), &String::from("1"))));
        assert!(preset
            .commands
            .contains(&PresetCommand::new(String::from("d"), &String::from("40"))));
        assert!(preset
            .commands
            .contains(&PresetCommand::exact(String::from("s"), 0.5)));
        assert!(preset
            .commands
            .contains(&PresetCommand::exact(String::from("r"), 2.0)));
    }

    #[test]
    fn preset_can_parse_line() {
        let sample_line = String::from(get_sample_preset_line());

        let preset = Preset::process_line(&sample_line).unwrap();

        assert_eq!(preset.name, String::from("Pulser"));
        assert!(preset.commands.contains(&PresetCommand::new(
            String::from("lwidth"),
            &String::from("500")
        )));
        assert!(preset
            .commands
            .contains(&PresetCommand::exact(String::from("delay"), 170.0)));
    }

    #[test]
    fn preset_can_parse_line_from_blob() {
        let sample_line = get_sample_preset_blob();

        let preset = Preset::get_nth_line(&sample_line, 0).expect("couln't get 1st line from blob");

        assert_eq!(preset.name, String::from("Bizarrely Wavering"));
        assert!(preset
            .commands
            .contains(&PresetCommand::exact(String::from("w"), 4.0)));
        assert!(preset
            .commands
            .contains(&PresetCommand::exact(String::from("lfo"), 1.0)));
    }

    #[test]
    fn preset_command_can_create() {
        let op = String::from("a");
        let val = String::from("0.5");

        let cmd = PresetCommand::new(op.clone(), &val);

        assert!(cmd.opcode == op);
    }

    #[test]
    fn preset_command_can_parse() {
        let op = String::from("d");
        let val = String::from("0.5");

        let cmd = PresetCommand::new(op, &val);

        assert_eq!(cmd.value, Some(0.5));
    }

    #[test]
    fn preset_command_can_distinguish() {
        let op = String::from("s");
        assert_ne!(
            PresetCommand::new(op, &String::from("0.1")).value,
            Some(0.9)
        );
        let op = String::from("r");
        assert_ne!(
            PresetCommand::new(op, &String::from("-0.7")).value,
            Some(2.0)
        );
    }

    #[test]
    fn preset_can_rewrite_data() {
        let name = String::from("Preset Name");
        let sample_data = String::from("a:1;d:40;s:0.5;r:2");

        let preset = Preset::parse(name.clone(), &sample_data);
        assert_eq!(preset.to_string(), format!("{}|{}", name, sample_data));
    }

    #[test]
    fn preset_can_write_data() {
        let name = String::from("Preset Name");
        let sample_data = String::from("a:1;d:40;s:0.5;r:2");
        let mut commands = vec![];

        commands.push(PresetCommand::exact(String::from("a"), 1.0));
        commands.push(PresetCommand::exact(String::from("d"), 40.0));
        commands.push(PresetCommand::exact(String::from("s"), 0.5));
        commands.push(PresetCommand::exact(String::from("r"), 2.0));

        let preset = Preset::new(name.clone(), commands);
        assert_eq!(preset.to_string(), format!("{}|{}", name, sample_data));
    }
}