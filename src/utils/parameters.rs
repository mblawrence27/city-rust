use enum_map::{Enum, EnumMap};

use crate::utils::state::{BoundedState, InitailState, PresetState, StateKeys};
use crate::utils::state::mappings::{both, map, PresetMapping};

use tranformations::*;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash, Enum)]
pub enum R {
    Attack,
    AmpLFOrate,
    AmpLFOwidth,
    Decay,
    DelayWet,
    FMScale,
    Frequency,
    Gain,
    GeneralAtten,
    Harmonic2Gain,
    HarmonicsControl,
    HarmonicFix,
    HarmonicFunction,
    HarmonicPhase,
    HarmonicV1,
    HPFCutoff,
    LPF,
    LPFattack,
    LPFceiling,
    LPFenvelope,
    LPFfloor,
    LPFmodrate,
    LPFrelease,
    LPFwidth,
    MaxHarmonic,
    MaxVelocity,
    Pitchmod,
    PitchmodWidth,
    PitchBendFactor,
    Release,
    ReverbWet,
    SubOscGain,
    Sustain,
    WFunction,
}

impl<T: Default + Copy> StateKeys<T> for R {}

// make into lazy static structure
impl InitailState<f64> for R {
    fn initial() -> EnumMap<Self, f64> {
        enum_map! {
            R::Attack            => 30.0,
            R::AmpLFOrate        => 30.0,
            R::AmpLFOwidth       => 0.0,
            R::Decay             => 50.0,
            R::DelayWet          => 0.5,
            R::FMScale           => 1.0,      // bool
            R::Frequency         => 440.0,
            R::Gain              => 0.2,
            R::GeneralAtten      => 0.3,
            R::Harmonic2Gain     => 0.0,
            R::HarmonicsControl  => 1.0,
            R::HarmonicFix       => 0.0,      // bool
            R::HarmonicFunction  => 0.0,
            R::HarmonicPhase     => 0.0,
            R::HarmonicV1        => 1.0,      // bool
            R::HPFCutoff         => 0.0,
            R::LPF               => 1200.0,
            R::LPFattack         => 180.0,
            R::LPFceiling        => 5000.0,
            R::LPFenvelope       => 0.0,      // bool
            R::LPFfloor          => 300.0,
            R::LPFmodrate        => 0.0,
            R::LPFrelease        => 30.0,
            R::LPFwidth          => 500.0,
            R::MaxHarmonic       => 5.0,
            R::MaxVelocity       => 127.0,
            R::Pitchmod          => 0.0,
            R::PitchmodWidth     => 0.0,
            R::PitchBendFactor   => 1.0,
            R::Release           => 20.0,
            R::ReverbWet         => 0.32,
            R::SubOscGain        => 0.0,
            R::Sustain           => 0.55,
            R::WFunction         => 0.0,      // int
        }
    }
}

// make into lazy static structure
impl PresetState<f64> for R {
    fn mapping() -> EnumMap<Self, PresetMapping<f64>> {
        enum_map! {
            R::AmpLFOrate         =>  map(Some("arate"),   None),
            R::AmpLFOwidth        =>  map(Some("awidth"),  None),
            R::Attack             =>  map(Some("a"),       None),
            R::Decay              =>  map(Some("d"),       None),
            R::DelayWet           =>  map(Some("dwet"),    None),
            R::FMScale            =>  map(None,            None),
            R::Frequency          =>  map(None,            None),
            R::GeneralAtten       => both(Some("g"),       Some(transform_from_db()), Some(transform_to_db())),
            R::Gain               =>  map(None,            None),
            R::Harmonic2Gain      =>  map(Some("h2"),      None),
            R::HarmonicFix        =>  map(Some("hf"),      None),
            R::HarmonicFunction   =>  map(Some("hw"),      Some(transform_to_locked_range(0, 5))),
            R::HarmonicPhase      =>  map(Some("hp"),      None),
            R::HarmonicsControl   =>  map(Some("hc"),      None),
            R::HarmonicV1         =>  map(Some("hv1"),     Some(transform_to_bool())),
            R::HPFCutoff          =>  map(Some("hpf"),     None),
            R::LPF                =>  map(Some("lpf"),     None),
            R::LPFattack          =>  map(Some("la"),      None),
            R::LPFceiling         =>  map(Some("lc"),      None),
            R::LPFenvelope        =>  map(Some("lpfenv"),  Some(transform_to_bool())),
            R::LPFfloor           =>  map(Some("lf"),      None),
            R::LPFmodrate         =>  map(Some("lfo"),     None),
            R::LPFrelease         =>  map(Some("lr"),      None),
            R::LPFwidth           =>  map(Some("lwidth"),  None),
            R::MaxHarmonic        =>  map(Some("h"),       Some(transform_trunc())),
            R::MaxVelocity        =>  map(None,            None),
            R::PitchBendFactor    =>  map(None,            None),
            R::Pitchmod           =>  map(Some("prate"),   None),
            R::PitchmodWidth      =>  map(Some("pwidth"),  None),
            R::Release            =>  map(Some("r"),       None),
            R::ReverbWet          =>  map(Some("rwet"),    None),
            R::SubOscGain         =>  map(Some("sub"),     None),
            R::Sustain            =>  map(Some("s"),       None),
            R::WFunction          =>  map(Some("w"),       Some(transform_to_locked_range(0, 5))), // TODO: un-magic
            // R::BaseFrequency     =>  map(Some("b"),       Some(transform_octave_to_freq)),
            // R::DelayTime         =>  map(Some("delay"),   None),
            // R::FilterDrive       =>  map(Some("fd"),      None),
            // R::FilterFeedback    =>  map(Some("ffb"),     None),
            // R::Pitchmodwidth     =>  map(Some("pwidth"),  None),

            // _                   => (None,        None)
        }
    }
}

// make into lazy static structure
impl BoundedState<f64> for R {
    fn bounds() -> EnumMap<Self, Option<(f64, f64)>> {
        const UNIT: (f64, f64) = (0.0, 0.0);

        enum_map! {
            R::AmpLFOrate         => Some((0.0, 440.0)),
            R::AmpLFOwidth        => Some((0.0, 1.0)),
            R::Attack             => Some((1.0, 1500.0)),
            R::Decay              => Some((5.0, 1500.0)),
            R::DelayWet           => Some((0.0, 1.1)),
            R::FMScale            => Some(UNIT),
            R::Frequency          => Some(UNIT),
            R::GeneralAtten       => Some((-48.0, 6.0)),
            R::Gain               => None,
            R::Harmonic2Gain      => None,
            R::HarmonicFix        => Some(UNIT),
            R::HarmonicFunction   => None,
            R::HarmonicPhase      => Some(UNIT),
            R::HarmonicsControl   => None,
            R::HarmonicV1         => Some(UNIT),
            R::HPFCutoff          => Some((5.0, 21000.0)),
            R::LPF                => Some((5.0, 21000.0)),
            R::LPFattack          => Some((5.0, 1500.0)),
            R::LPFceiling         => Some((5.0, 21000.0)),
            R::LPFenvelope        => Some(UNIT),
            R::LPFfloor           => Some((5.0, 21000.0)),
            R::LPFmodrate         => Some((0.0, 20.0)),
            R::LPFrelease         => Some((5.0, 1500.0)),
            R::LPFwidth           => Some((10.0, 15000.0)),
            R::MaxHarmonic        => None,
            R::MaxVelocity        => None,
            R::PitchBendFactor    => None,
            R::Pitchmod           => None,
            R::PitchmodWidth      => Some((0.0, 0.2)),
            R::Release            => Some((2.0, 1500.0)),
            R::ReverbWet          => None,
            R::SubOscGain         => Some((0.0, 2.0)),
            R::Sustain            => Some(UNIT),
            R::WFunction          => None,
            // R::BaseFrequency     =>  map(Some("b"),       Some(transform_octave_to_freq)),
            // R::DelayTime         =>  map(Some("delay"),   None),
            // R::FilterDrive       =>  map(Some("fd"),      None),
            // R::FilterFeedback    =>  map(Some("ffb"),     None),
            // R::Pitchmodwidth     =>  map(Some("pwidth"),  None),

            // _                   => (None,        None)
        }
    }
}

pub mod tranformations {
    use crate::utils::state::mappings::ValueTransformFn;

    pub fn transform_octave_to_freq() -> ValueTransformFn<f64> {
        Box::new(|value| {
            440.0 * f64::from(2 << value as u64)
        })
    }
    pub fn transform_trunc() -> ValueTransformFn<f64> {
        Box::new(|value| {
            value.trunc()
        })
    }
    pub fn transform_to_bool() -> ValueTransformFn<f64> {
        Box::new(|value| {
            if value == 0.0 { 0.0 } else { 1.0 }
        })
    }
    pub fn transform_to_db() -> ValueTransformFn<f64> {
        Box::new(|value| {
            value.log(10.0_f64) * 10.0
        })
    }
    pub fn transform_from_db() -> ValueTransformFn<f64> {
        Box::new(|value| {
            10.0_f64.powf(value / 10.0)
        })
    }
    pub fn transform_to_int_range(max_value: u32) -> ValueTransformFn<f64> {
        Box::new(move |value| {
            (value * f64::from(max_value)).trunc()
        })
    }
    pub fn transform_to_locked_range(min_value: u32, max_value: u32) -> ValueTransformFn<f64> {
        Box::new(move |value| {
            let clamped = std::cmp::max(value as u32, min_value);
            let clamped = std::cmp::min(clamped as u32, max_value);
            (f64::from(clamped)).trunc()
        })
    }
}
