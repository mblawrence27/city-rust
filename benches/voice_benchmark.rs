#[macro_use]
extern crate criterion;

use criterion::Criterion;
// use criterion::black_box;

use cityrust::synth::voice::SynthVoice;
use cityrust::utils::loader;
// use cityrust::utils::parameters::R;
use cityrust::utils::state::StateManager;

fn criterion_benchmark(c: &mut Criterion) {
    let sr = 44100.0;
    let mut buffer = [0.0; 1024];
    let mut b = StateManager::new();
    loader::load_defaults(&mut b);
    let mut voice = SynthVoice::new(sr, Default::default(), b);
    c.bench_function("mono synth 1024",
        move |b| b.iter(|| voice.read(&mut buffer))
    );
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);