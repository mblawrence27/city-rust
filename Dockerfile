FROM rustlang/rust:nightly-slim

WORKDIR /usr/src/myapp
COPY . .

RUN cargo build --release

#CMD ["cargo", "run"]
#CMD ["echo", "cargo build --release && ./target/release/src", ">", "go.sh"]
#CMD ["chmod", "+x", "go.sh"]
#CMD ["/bin/bash"]
CMD ["./target/release/src"]
