pub mod delay_fx;
pub mod reverb_fx;
pub mod filter_fx;
pub mod pinger;

pub mod prelude {
    pub use vst::buffer::AudioBuffer;
    pub use vst::plugin::{Category, Info, Plugin};

    #[derive(PartialEq)]
    pub struct ParamInfo {
        pub name: String,
        pub value: f32,
        pub min_value: f32,
        pub max_value: f32,
    }

    impl ParamInfo {
        pub fn value_to_unit_range(info: &ParamInfo) -> f32 {
            let range = info.max_value - info.min_value;
            (info.value - info.min_value) / range
        }

        pub fn value_from_unit_range(info: &ParamInfo, new_unit_value: f32) -> f32 {
            let range = info.max_value - info.min_value;
            (new_unit_value * range) + info.min_value
        }
    }
}
