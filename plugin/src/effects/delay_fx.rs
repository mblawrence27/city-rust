use crate::effects::prelude::*;

use cityrust::dsp::butterworth::ButterworthFilter;
use cityrust::dsp::delay::Delay;
use cityrust::dsp::reverb::Reverb;
use cityrust::dsp::simple::rc::SimpleHighPass;

const AUDIO_IO_CHANNELS_COUNT: usize = 2;

type MultiChannel<T> = Option<[T; AUDIO_IO_CHANNELS_COUNT]>;

pub struct CityDelayPlugin {
    delay: MultiChannel<Delay>,
    filter: MultiChannel<ButterworthFilter>,
    hpf: MultiChannel<SimpleHighPass>,
    in_gain: f64,
    hpf_cutoff: f64,
    reverb: MultiChannel<Reverb>,
    reverb_wet: f64, 
    reverb_out: f64,
    delay_wet: f64,
    params: Vec<Params>,
}

impl CityDelayPlugin {
    fn generate_delay(rate: i32) -> Option<Delay> {
        let mut delay = Delay::new(rate, rate as usize, 6.0);
        delay.set_flutter_params(0.009, 9.5);
        Some(delay)
    }
    fn generate_filter(poles: usize, sr: f64, fc: f64) -> Option<ButterworthFilter> {
        let mut filter = ButterworthFilter::new(poles, sr);
        filter.set_cutoff(fc);
        Some(filter)
    }
    fn generate_hpf(sr: f64) -> Option<SimpleHighPass> {
        let hpf = SimpleHighPass::new(sr);
        Some(hpf)
    }
    fn generate_reverb(sr: f64) -> Option<Reverb> {
        let vecs = vec![3501, 130, 43, 14, 4];
        let vecs: Vec<usize> = vecs
            .iter()
            .flat_map(|l| vec![*l, (*l / 2) as usize + 1])
            .collect();
        let reverb = Reverb::new(sr as i32, vecs);
        Some(reverb)
    }
    
    fn init_delays(rate: i32) -> MultiChannel<Delay> {
        Some([
            CityDelayPlugin::generate_delay(rate).unwrap(),
            CityDelayPlugin::generate_delay(rate).unwrap(),
        ])
    }
    fn init_filters(poles: usize, sr: f64, fc: f64) -> MultiChannel<ButterworthFilter> {
        Some([
            CityDelayPlugin::generate_filter(poles, sr, fc).unwrap(),
            CityDelayPlugin::generate_filter(poles, sr, fc).unwrap(),
        ])
    }
    fn init_hpf_filters(sr: f64) -> MultiChannel<SimpleHighPass> {
        Some([
            CityDelayPlugin::generate_hpf(sr).unwrap(),
            CityDelayPlugin::generate_hpf(sr).unwrap(),
        ])
    }
    fn init_reverb(sr: f64) -> MultiChannel<Reverb> {
        Some([
            CityDelayPlugin::generate_reverb(sr).unwrap(),
            CityDelayPlugin::generate_reverb(sr).unwrap(),
        ])
    }
}

enum Params {
    InputGain(ParamInfo),
    Wet(ParamInfo),
    Length(ParamInfo),
    Flutter(ParamInfo),
    FlutterSpeed(ParamInfo),
    Feedback(ParamInfo),
    LPFCutoff(ParamInfo),
    HPFCutoff(ParamInfo),
    Reverb(ParamInfo),
    ReverbOut(ParamInfo),
}

impl Plugin for CityDelayPlugin {
    fn get_info(&self) -> Info {
        Info {
            name: String::from("CityDelay"),
            unique_id: 96383,
            inputs: AUDIO_IO_CHANNELS_COUNT as i32,
            outputs: AUDIO_IO_CHANNELS_COUNT as i32,
            parameters: self.params.len() as i32,
            category: Category::Effect,

            ..Default::default()
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        let rate = rate as i32;
        self.delay = CityDelayPlugin::init_delays(rate);
        self.filter = CityDelayPlugin::init_filters(4, f64::from(rate), 1000.0);
        self.hpf = CityDelayPlugin::init_hpf_filters(f64::from(rate));
        self.reverb = CityDelayPlugin::init_reverb(f64::from(rate));
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        if let (Some(delay), Some(filter), Some(hpf), Some(reverb))
            = (self.delay.as_mut(), self.filter.as_mut(), self.hpf.as_mut(), self.reverb.as_mut())
        {
            let both = buffer.zip().enumerate();

            for (idx, (input_channel, output_channel))in both {
                let io_channel = input_channel.iter().zip(output_channel);

                for (input_sample, output_sample) in io_channel {
                    let input_sample = f64::from(*input_sample) * self.in_gain;
                    let return_sample = delay[idx].return_sample() * self.delay_wet;
                    let mut sample = return_sample + input_sample * (1.0 - self.delay_wet);
                    sample = filter[idx].process(sample);
                    sample = hpf[idx].process(sample, self.hpf_cutoff);
                    sample = reverb[idx].get_sample(sample, self.reverb_wet);
                    delay[idx].send_sample(sample);
                    let processed = (0.5 - self.reverb_out * 0.5) * (return_sample + input_sample);
                    *output_sample = (processed + sample * self.reverb_out * 0.5) as f32;
                }
            }
        }
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::InputGain(info) => format!("{:.3}", info.value),
            Params::Wet(info) => format!("{:.3}", info.value),
            Params::Length(info) => format!("{:.3}", info.value),
            Params::Flutter(info) => format!("{:.3}", info.value),
            Params::FlutterSpeed(info) => format!("{:.3}", info.value),
            Params::Feedback(info) => format!("{:.3}", info.value),
            Params::LPFCutoff(info) => format!("{:.3}", info.value),
            Params::HPFCutoff(info) => format!("{:.3}", info.value),
            Params::Reverb(info) => format!("{:.3}", info.value),
            Params::ReverbOut(info) => format!("{:.3}", info.value),
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::InputGain(info) => info.name.clone(),
            Params::Wet(info) => info.name.clone(),
            Params::Length(info) => info.name.clone(),
            Params::Flutter(info) => info.name.clone(),
            Params::FlutterSpeed(info) => info.name.clone(),
            Params::Feedback(info) => info.name.clone(),
            Params::LPFCutoff(info) => info.name.clone(),
            Params::HPFCutoff(info) => info.name.clone(),
            Params::Reverb(info) => info.name.clone(),
            Params::ReverbOut(info) => info.name.clone(),
        }
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match &self.params[index as usize] {
            Params::InputGain(info) => ParamInfo::value_to_unit_range(info),
            Params::Wet(info) => ParamInfo::value_to_unit_range(info),
            Params::Length(info) => ParamInfo::value_to_unit_range(info),
            Params::Flutter(info) => ParamInfo::value_to_unit_range(info),
            Params::FlutterSpeed(info) => ParamInfo::value_to_unit_range(info),
            Params::Feedback(info) => ParamInfo::value_to_unit_range(info),
            Params::LPFCutoff(info) => ParamInfo::value_to_unit_range(info),
            Params::HPFCutoff(info) => ParamInfo::value_to_unit_range(info),
            Params::Reverb(info) => ParamInfo::value_to_unit_range(info),
            Params::ReverbOut(info) => ParamInfo::value_to_unit_range(info),
        }
    }

    fn set_parameter(&mut self, index: i32, value: f32) {
        match &mut self.params[index as usize] {
            Params::InputGain(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                self.in_gain = f64::from(info.value);
            }
            Params::Wet(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                self.delay_wet = f64::from(info.value);
            }
            Params::Length(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if let Some(delays) = self.delay.as_mut() {
                    for delay in delays {
                        delay.set_tape_length(f64::from(info.value));
                    }
                }
            }
            Params::Flutter(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if let Some(delays) = self.delay.as_mut() {
                    for delay in delays {
                        let flutter = delay.get_flutter_params();
                        delay.set_flutter_params(f64::from(info.value), flutter.1);
                    }
                }
            }
            Params::FlutterSpeed(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if let Some(delays) = self.delay.as_mut() {
                    for delay in delays {
                        let flutter = delay.get_flutter_params();
                        delay.set_flutter_params(flutter.0, f64::from(info.value));
                    }
                }
            }
            Params::Feedback(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if let Some(delays) = self.delay.as_mut() {
                    for delay in delays {
                        delay.set_feedback(f64::from(info.value));
                    }
                }
            }
            Params::LPFCutoff(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if let Some(filters) = self.filter.as_mut() {
                    for filter in filters {
                        filter.set_cutoff(f64::from(info.value));
                    }
                }
            }
            Params::HPFCutoff(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                self.hpf_cutoff = f64::from(info.value);
            }
            Params::Reverb(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                self.reverb_wet = f64::from(info.value);
            }
            Params::ReverbOut(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                self.reverb_out = f64::from(info.value);
            }
        }
    }
}

impl Default for CityDelayPlugin {
    fn default() -> CityDelayPlugin {
        let init_in_gain = 0.8;
        let init_hpf_cutoff = 0.0;
        let init_wet = 0.4;
        let init_reverb_amt = 0.0;
        let init_reverb_out = 0.0;

        CityDelayPlugin {
            in_gain: init_in_gain,
            delay: None,
            filter: None,
            hpf: None,
            hpf_cutoff: init_hpf_cutoff,
            reverb: None,
            reverb_wet: init_reverb_amt,
            reverb_out: init_reverb_out,
            delay_wet: init_wet,
            params: vec![
                Params::InputGain(ParamInfo {
                    name: String::from("Input Gain"),
                    value: init_in_gain as f32,
                    min_value: 0.0,
                    max_value: 1.0,
                }),
                Params::Wet(ParamInfo {
                    name: String::from("Wet"),
                    value: init_wet as f32,
                    min_value: 0.0,
                    max_value: 1.0,
                }),
                Params::Length(ParamInfo {
                    name: String::from("Time"),
                    value: 0.4,
                    min_value: 0.175,
                    max_value: 1.0,
                }),
                Params::Flutter(ParamInfo {
                    name: String::from("Flutter"),
                    value: 0.009,
                    min_value: 0.0,
                    max_value: 0.055,
                }),
                Params::FlutterSpeed(ParamInfo {
                    name: String::from("Flutter Speed"),
                    value: 9.5,
                    min_value: 0.0,
                    max_value: 15.0,
                }),
                Params::Feedback(ParamInfo {
                    name: String::from("Feedback"),
                    value: 0.0,
                    min_value: 0.0,
                    max_value: 0.99,
                }),
                Params::LPFCutoff(ParamInfo {
                    name: String::from("LPF"),
                    value: 1000.0,
                    min_value: 20.0,
                    max_value: 20000.0,
                }),
                Params::HPFCutoff(ParamInfo {
                    name: String::from("HPF"),
                    value: init_hpf_cutoff as f32,
                    min_value: 0.0,
                    max_value: 20000.0,
                }),
                Params::Reverb(ParamInfo {
                    name: String::from("Reverb Mix"),
                    value: init_reverb_amt as f32,
                    min_value: 0.0,
                    max_value: 0.99,
                }),
                Params::ReverbOut(ParamInfo {
                    name: String::from("Reverb Out"),
                    value: init_reverb_out as f32,
                    min_value: 0.0,
                    max_value: 0.99,
                })
            ],
        }
    }
}