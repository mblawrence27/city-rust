use crate::effects::prelude::*;
use cityrust::dsp::butterworth::ButterworthFilter;

enum Params {
    Cutoff(ParamInfo),
}

pub struct CityButterworthPlugin {
    filter: Option<ButterworthFilter>,
    params: Vec<Params>,
}

impl CityButterworthPlugin {
    fn init_filter(poles: usize, sr: f64, fc: f64) -> Option<ButterworthFilter> {
        let mut filter = ButterworthFilter::new(poles, sr);
        filter.set_cutoff(fc);
        Some(filter)
    }
}

impl Plugin for CityButterworthPlugin {
    fn get_info(&self) -> Info {
        Info {
            name: String::from("CityFilter"),
            unique_id: 96382,
            inputs: 1,
            outputs: 1,
            parameters: self.params.len() as i32,
            category: Category::Effect,

            ..Default::default()
        }
    }

    fn init(&mut self) {
        self.filter = CityButterworthPlugin::init_filter(4, 44100.0, 1000.0);
    }

    fn set_sample_rate(&mut self, rate: f32) {
        self.filter = CityButterworthPlugin::init_filter(4, f64::from(rate), 1000.0);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let both = buffer.zip();
        let filter: &mut ButterworthFilter = self.filter.as_mut().unwrap();

        for (input_channels, output_channels) in both {
            let io_channels = input_channels.iter().zip(output_channels);
            for (input_sample, output_sample) in io_channels {
                *output_sample = filter.process(f64::from(*input_sample)) as f32;
            }
        }
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::Cutoff(info) => format!("{:.3}", info.value),
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::Cutoff(info) => info.name.clone(),
        }
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match &self.params[index as usize] {
            Params::Cutoff(info) => ParamInfo::value_to_unit_range(info),
        }
    }

    fn set_parameter(&mut self, index: i32, value: f32) {
        match &mut self.params[index as usize] {
            Params::Cutoff(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if self.filter.is_some() {
                    let filter: &mut ButterworthFilter = self.filter.as_mut().unwrap();
                    filter.set_cutoff(f64::from(info.value));
                }
            }
        }
    }
}

impl Default for CityButterworthPlugin {
    fn default() -> CityButterworthPlugin {
        CityButterworthPlugin {
            filter: None,
            params: vec![Params::Cutoff(ParamInfo {
                name: String::from("Cutoff"),
                value: 1000.0,
                min_value: 20.0,
                max_value: 20000.0,
            })],
        }
    }
}