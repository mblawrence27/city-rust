use crate::effects::prelude::*;

use cityrust::dsp::reverb::Reverb;

enum Params {
    Flutter(ParamInfo),
    FlutterSpeed(ParamInfo),
    // Feedback(ParamInfo),
}

pub struct CityVerbPlugin {
    reverb: Option<Reverb>,
    params: Vec<Params>,
}

impl Plugin for CityVerbPlugin {
    fn get_info(&self) -> Info {
        Info {
            name: String::from("CityVerb"),
            unique_id: 96382,
            inputs: 1,
            outputs: 1,
            parameters: self.params.len() as i32,
            category: Category::Effect,

            ..Default::default()
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        let vecs = vec![3501, 130, 43, 14, 4];
        let vecs: Vec<usize> = vecs
            .iter()
            .flat_map(|l| vec![*l, (*l / 2) as usize + 1])
            .collect();
        let reverb = Reverb::new(rate as i32, vecs);
        self.reverb = Some(reverb);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let both = buffer.zip();

        for (input_channels, output_channels) in both {
            let io_channels = input_channels.iter().zip(output_channels);
            for (input_sample, output_sample) in io_channels {
                *output_sample = *input_sample;
            }
        }
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::Flutter(info) => format!("{:.3}", info.value),
            Params::FlutterSpeed(info) => format!("{:.3}", info.value),
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::Flutter(info) => info.name.clone(),
            Params::FlutterSpeed(info) => info.name.clone(),
        }
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match &self.params[index as usize] {
            Params::Flutter(info) => ParamInfo::value_to_unit_range(info),
            Params::FlutterSpeed(info) => ParamInfo::value_to_unit_range(info),
        }
    }

    fn set_parameter(&mut self, index: i32, value: f32) {
        match &mut self.params[index as usize] {
            Params::Flutter(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if self.reverb.is_some() {
                    let reverb: &mut Reverb = self.reverb.as_mut().unwrap();
                    reverb.set_nested_delays_flutter(f64::from(info.value));
                }
            }
            Params::FlutterSpeed(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                if self.reverb.is_some() {
                    let reverb: &mut Reverb = self.reverb.as_mut().unwrap();
                    reverb.set_nested_delays_flutter_speed(f64::from(info.value));
                }
            }
        }
    }
}

impl Default for CityVerbPlugin {
    fn default() -> CityVerbPlugin {
        CityVerbPlugin {
            reverb: None,
            params: vec![
                Params::Flutter(ParamInfo {
                    name: String::from("Flutter"),
                    value: 0.0,
                    min_value: 0.00,
                    max_value: 0.1,
                }),
                Params::FlutterSpeed(ParamInfo {
                    name: String::from("Flutter Speed"),
                    value: 0.0,
                    min_value: 0.00,
                    max_value: 20.0,
                }),
            ],
        }
    }
}