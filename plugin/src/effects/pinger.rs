use crate::effects::prelude::*;

use cityrust::utils::loader;
use cityrust::utils::parameters::R;
use cityrust::utils::preset::Preset;
use cityrust::utils::state::{StateManager, StateKeys};
use cityrust::synth::voice::SynthVoice;

#[derive(PartialEq)]
enum Params {
    Freq(ParamInfo),
}

pub struct CityPinger {
    osc1: Option<SynthVoice>,
    manager: Option<StateManager<R, f64>>,
    sr: f64,
    fund_freq: f64,
    counter: usize,
    params: Vec<Params>,
    presets: Vec<Preset>,
    preset_idx: usize,
}

impl CityPinger {
    fn load_preset_str() -> String {
        include_str!("userpresets.sdp").to_string()
    }

    fn load_presets(input_presets_sdp: String) -> Vec<Preset> {
        Preset::get_lines(&input_presets_sdp)
    }

    fn init_state(inital: &Preset) -> Option<StateManager<R, f64>> {
        let mut manager = StateManager::new();
        loader::load_defaults(&mut manager);
        let _ = loader::load_commands(&mut manager, &inital.commands);

        Some(manager)
    }

    fn init_voice(sr: f64, manager: &StateManager<R, f64>) -> Option<SynthVoice> {
        let b = manager.clone();
        let mut voice = SynthVoice::new(sr, 0, b);
        voice.note_on();

        Some(voice)
    }
    
    fn reload_params_values(&mut self) {
        for param in &mut self.params {
            match param {
                Params::Freq(info)      => info.value = R::Frequency.get(self.manager.as_ref().unwrap()) as f32,
            }
        }
    }
}

impl Plugin for CityPinger {
    fn get_info(&self) -> Info {
        Info {
            name: String::from("CitySimpleSynth"),
            unique_id: 96383,
            inputs: 0,
            outputs: 1,
            parameters: self.params.len() as i32,
            presets: self.presets.len() as i32,
            category: Category::Synth,

            ..Default::default()
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        self.sr = f64::from(rate);

        self.manager = CityPinger::init_state(&self.presets[0]);
        self.osc1 = CityPinger::init_voice(f64::from(rate), &self.manager.as_mut().unwrap());
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let (_, output) = buffer.split();
        let osc1 = self.osc1.as_mut().unwrap();

        let mut single_sample_buffer = [0.0; 16];
        let buffer_len = single_sample_buffer.iter().len() as u32;

        for output_channel in output.into_iter() {
            for output_sample in output_channel {

                let idx = self.counter % buffer_len as usize;
                if idx == 0 {
                    osc1.read(&mut single_sample_buffer);
                }
                *output_sample = single_sample_buffer[idx] as f32;
                self.counter += 1;
            }
        }
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::Freq(info)      => format!("{:.3} Hz", info.value),
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        match &self.params[index as usize] {
            Params::Freq(info)      => info.name.clone(),
        }
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match &self.params[index as usize] {
            Params::Freq(info)      => ParamInfo::value_to_unit_range(info),
        }
    }

    fn set_parameter(&mut self, index: i32, value: f32) {
        match &mut self.params[index as usize] {
            Params::Freq(info) => {
                info.value = ParamInfo::value_from_unit_range(info, value);
                self.fund_freq = f64::from(info.value);
                self.manager.as_mut().unwrap().set(R::Frequency, self.fund_freq);
            },
        }
    }

    fn get_preset_num(&self) -> i32 {
        self.preset_idx as i32
    }

    fn get_preset_name(&self, preset: i32) -> String {
        self.presets[preset as usize].name.clone()
    }
    
    fn change_preset(&mut self, preset: i32) {
        self.preset_idx = preset as usize;
        let preset = &self.presets[self.preset_idx];
        let manager = self.manager.as_mut().unwrap();
        let _ = loader::load_commands(manager, &preset.commands);
        self.reload_params_values();
    }
    
    fn set_preset_name(&mut self, name: String) {
        self.presets[self.preset_idx].name = name;
    }
}

impl Default for CityPinger {
    fn default() -> CityPinger {
        let fund_freq = 440.0;
        
        let input_presets_sdp = CityPinger::load_preset_str();
        let presets = CityPinger::load_presets(input_presets_sdp);

        CityPinger {
            osc1: None,
            manager: None,
            presets,
            preset_idx: 0,
            counter: 0,
            fund_freq,
            sr: 44100.0,
            params: vec![
                Params::Freq(ParamInfo {
                    name: String::from("Frequency"),
                    value: fund_freq as f32,
                    min_value: 80.0,
                    max_value: 1000.0,
                }),
            ],
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn pinger_fx_can_produce_samples() {
        const SAMPLES_COUNT: usize = 1024;

        let mut plugin = CityPinger::default();
        plugin.set_sample_rate(41000.0);

        plugin.set_parameter(plugin.params.iter()
            .position(|param| match param { Params::Freq(_) => true })
            .unwrap() as i32, 1.0);

        plugin.manager.as_mut().unwrap().set(R::WFunction, 0.0);

        let mut sampls = [0.0; SAMPLES_COUNT as usize].to_vec();

        let mut buffer = unsafe {
            AudioBuffer::from_raw(
                0,
                1,
                std::ptr::null(),
                [sampls.as_mut_ptr()].as_mut_ptr(),
                SAMPLES_COUNT
            )
        };

        let times = 32;
        for i in 0..times {
            plugin.set_parameter(plugin.params.iter()
                .position(|param| match param { Params::Freq(_) => true })
                .unwrap() as i32, i as f32 / times as f32);
            plugin.process(&mut buffer);
        }

        sampls.len();
    }
}