#![allow(unused_imports)]

extern crate cityrust;
#[macro_use] extern crate vst;

mod effects;
mod synth;

pub use crate::effects::delay_fx::CityDelayPlugin;
pub use crate::effects::filter_fx::CityButterworthPlugin;
pub use crate::effects::reverb_fx::CityVerbPlugin;
pub use crate::effects::pinger::CityPinger;
pub use crate::synth::CitySynth;

#[cfg(feature = "verb")]
plugin_main!(CityVerbPlugin);

#[cfg(feature = "delay")]
plugin_main!(CityDelayPlugin);

#[cfg(feature = "filter")]
plugin_main!(CityButterworthPlugin);

#[cfg(feature = "ping")]
plugin_main!(CityPinger);

#[cfg(feature = "CitySynth")]
plugin_main!(CitySynth);