use std::f64::consts::PI;
use std::thread::{self, JoinHandle};

use vst::api::{Events, Supported};
use vst::buffer::AudioBuffer;
use vst::event::Event;
use vst::plugin::{CanDo, Category, Info, Plugin};

use cityrust::synth::poly::CitySynth as PolySynth;
use cityrust::synth::sender::{KeyEventHandler, MidiData};
use cityrust::utils::loader;
use cityrust::utils::parameters::R;
use cityrust::utils::preset::Preset;
use cityrust::utils::state::StateManager;

use crate::effects::prelude::*;

const MICRO_BUFFER_LEN: usize = 32;

pub struct CitySynth {
    poly: PolySynth,
    bucket: StateManager<R, f64>,
    events: KeyEventHandler<MidiData>,
    presets: Vec<Preset>,
    preset_idx: usize,
    thread: JoinHandle<()>,
    counter: usize,
}

impl CitySynth {
    pub fn new(sr: f64, voices_count: usize) -> Self {
        let mut bucket = StateManager::new();
        loader::load_defaults(&mut bucket);

        let input_presets_sdp = CitySynth::load_preset_str();
        let presets = CitySynth::load_presets(input_presets_sdp);

        let (synth, events_handler) = PolySynth::new(sr, voices_count, bucket.clone());

        let thread = thread::spawn(move || {
            net::start_server()
        });

        Self {
            poly: synth,
            events: events_handler,
            bucket,
            thread,
            counter: 0,
            presets,
            preset_idx: 0
        }
    }

    fn process_midi_event(&mut self, data: [u8; 3]) {
        const MIDI_NOTE_OFF_BYTE: u8 = 128;
        const MIDI_NOTE_ON_BYTE: u8 = 144;
        match data[0] {
            MIDI_NOTE_OFF_BYTE => self.note_off(data[1]),
            MIDI_NOTE_ON_BYTE => self.note_on(data[1], data[2]),
            _ => (),
        }
    }

    fn note_on(&mut self, note: u8, vel: u8) {
        self.events.key_down(MidiData::new(note, vel));
        self.events.fire_events();
    }

    fn note_off(&mut self, note: u8) {
        self.events.key_up(MidiData::from(note));
        self.events.fire_events();
    }

    fn load_preset_str() -> String {
        include_str!("effects/userpresets.sdp").to_string()
    }

    fn load_presets(input_presets_sdp: String) -> Vec<Preset> {
        Preset::get_lines(&input_presets_sdp)
    }
}

impl Default for CitySynth {
    fn default() -> Self {
        Self::new(44100.0, 8)
    }
}

impl Plugin for CitySynth {
    fn get_info(&self) -> Info {
        Info {
            name: "CitySynth".to_string(),
            vendor: "Me".to_string(),
            unique_id: 6667,
            category: Category::Synth,
            inputs: 0,
            outputs: 1,
            parameters: 0,
            initial_delay: 0,
            presets: self.presets.len() as i32,
            ..Info::default()
        }
    }

    fn process_events(&mut self, events: &Events) {
        for event in events.events() {
            match event {
                Event::Midi(ev) => self.process_midi_event(ev.data),
                _ => (),
            }
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        // self.sample_rate = f64::from(rate);
        self.poly.set_buffer_len(MICRO_BUFFER_LEN);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let (_, output) = buffer.split();

        let mut single_sample_buffer = [0.0; MICRO_BUFFER_LEN];
        let buffer_len = single_sample_buffer.iter().len() as u32;

        for output_channel in output.into_iter() {
            for output_sample in output_channel {
                let idx = self.counter % buffer_len as usize;
                if idx == 0 {
                    self.poly.read(&mut single_sample_buffer);
                }
                *output_sample = single_sample_buffer[idx] as f32;
                self.counter += 1;
            }
        }
    }

    fn can_do(&self, can_do: CanDo) -> Supported {
        match can_do {
            CanDo::ReceiveMidiEvent => Supported::Yes,
            _ => Supported::Maybe,
        }
    }

    fn get_preset_num(&self) -> i32 {
        self.preset_idx as i32
    }

    fn get_preset_name(&self, preset: i32) -> String {
        self.presets[preset as usize].name.clone()
    }
    
    fn change_preset(&mut self, preset: i32) {
        self.preset_idx = preset as usize;
        let preset = &self.presets[self.preset_idx];
        let _ = loader::load_commands(&mut self.bucket, &preset.commands);
    }
    
    fn set_preset_name(&mut self, name: String) {
        self.presets[self.preset_idx].name = name;
    }
}

mod net {
    use std::io::Write;
    use std::net::{TcpListener, TcpStream};
    use std::thread::{self, JoinHandle};

    pub fn start_server() {
        let (ip_address, port) = ("0.0.0.0", "27017");
        let full_address = format!("{}:{}", ip_address, port);
        let listener = TcpListener::bind(&full_address).unwrap();

        println!("we've now bound to {}", full_address);
        loop {
            for stream in listener.incoming() {
                let stream = stream.unwrap();
                let _ = thread::spawn(|| {
                    handle_connection(stream);
                });
            }
        }
    }

    fn handle_connection(mut stream: TcpStream) {
        println!("we've got a connection");

        stream.write_fmt(format_args!("{:.*}", 2, 1.234567)).unwrap();
    }
}